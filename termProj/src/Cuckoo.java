import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/*
class HashFunction<T> {
    private static final Random ENGINE = new Random();
    private int rounds;

    public HashFunction() {
        this(1);
    }

    public HashFunction(int rounds) {
        this.rounds = rounds;
    }

    public int hash(Object key, int limit) {
        Random ENGINE = new Random();
        ENGINE.setSeed(key.hashCode());
        int h = ENGINE.nextInt(limit);
        for (int i = 1; i < this.rounds; i++) {
            h = ENGINE.nextInt(limit);
        }

        return h;
    }
}
*/

public class Cuckoo implements HashTable {
    volatile int capacity;
    volatile List<Integer>[][] table;
    final ReentrantLock[][] lock;
    //final HashFunction<Integer> hash0;
    //final HashFunction<Integer> hash1;




    final static int THRESHOLD = 2;
    final static int PROBE_SIZE = 4;
    final static int LIMIT = 4;

    public Cuckoo() {
        capacity = 200;
        table = (List<Integer>[][]) new java.util.ArrayList[2][capacity];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < capacity; j++) {
                table[i][j] = new ArrayList<Integer>(PROBE_SIZE);
            }
        }

        lock = new ReentrantLock[2][capacity];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < capacity; j++) {
                lock[i][j] = new ReentrantLock();
            }
        }

        //hash0 = new HashFunction<Integer>(2);
        //hash1 = new HashFunction<Integer>(3);
    }

    public Cuckoo(int size) {
        capacity = size;
        table = (List<Integer>[][]) new java.util.ArrayList[2][capacity];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < capacity; j++) {
                table[i][j] = new ArrayList<Integer>(PROBE_SIZE);
            }
        }

        lock = new ReentrantLock[2][capacity];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < capacity; j++) {
                lock[i][j] = new ReentrantLock();
            }
        }

        //hash0 = new HashFunction<Integer>(2);
        //hash1 = new HashFunction<Integer>(3);    
    }
/*
    private int hash(HashFunction<Integer> func, int key) {
        return func.hash((Integer)key, capacity);
    }
*/
    static int hash1(int key) {
        int h = ((Integer)key).hashCode();
        return h ^ (h >>> 16);
    }

    static int hash0(int key) {
        int h = ((Integer)key).hashCode();
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    public boolean remove(int x) {

        acquire(x);
        try {
            List<Integer> set0 = table[0][hash0(x) % capacity];

            if (set0.contains(x)) {
                set0.remove((Integer) x);
//                set0.remove(x);
//                System.out.println(x + "[][][]");
                return true;
            } else {
                List<Integer> set1 = table[1][hash1(x) % capacity];

                if (set1.contains(x)) {
                    set1.remove((Integer) x);
                    return true;
                }
            }
            return false;
        } finally {
            release(x);
        }
    }


    public boolean contains(int x) {
        acquire(x);
        try {
            List<Integer> set0 = table[0][hash0(x) % capacity];
            if (set0.contains(x)) {
                return true;
            } else {
                List<Integer> set1 = table[1][hash1(x) % capacity];
                if (set1.contains(x)) {
                    return true;
                }
            }
            return false;
        } finally {
            release(x);
        }
    }

    public boolean add(int x) {
        Integer y = null;
        acquire(x);
        int h0 = hash0(x) % capacity, h1 = hash1(x) % capacity;
        int i = -1, h = -1;
        boolean mustResize = false;
        try {
            //if (contains(x)) return false;
            List<Integer> set0 = table[0][h0];
            List<Integer> set1 = table[1][h1];
            if (set0.contains(x) || set1.contains(x)) 
                return false;
            if (set0.size() < THRESHOLD) {
                set0.add(x); return true;
            } else if (set1.size() < THRESHOLD) {
                set1.add(x); return true;
            } else if (set0.size() < PROBE_SIZE) {
                set0.add(x); i = 0; h = h0;
            } else if (set1.size() < PROBE_SIZE) {
                set1.add(x); i = 1; h = h1;
            } else {
                mustResize = true;
            }
        } finally {
            release(x);
        }
        if (mustResize) {
            resize(); add(x);
        } else if (!relocate(i, h)) {
            resize();
        }
        return true; // x must have been present
    }

    protected boolean relocate(int i, int hi) {
        int hj = 0;
        int j = 1 - i;
        for (int round = 0; round < LIMIT; round++) {
            List<Integer> iSet = table[i][hi];
            int y = iSet.get(0);
            switch (i) {
                case 0: hj = hash1(y) % capacity; break;
                case 1: hj = hash0(y) % capacity; break;
            }
            acquire(y);
            List<Integer> jSet = table[j][hj];
            try {
                if (iSet.remove((Integer)y)) {
                    if (jSet.size() < THRESHOLD) {
                        jSet.add(y);
                        return true;
                    } else if (jSet.size() < PROBE_SIZE) {
                        jSet.add(y);
                        i = 1 - i;
                        hi = hj;
                        j = 1 - j;
                    } else {
                        iSet.add(y);
                        return false;
                    }
                } else if (iSet.size() >= THRESHOLD) {
                    continue;
                } else {
                    return true;
                }
            } finally {
                release(y);
            }
        }
        return false;
    }

    public void acquire(int x) {
        lock[0][hash0(x) % lock[0].length].lock();
        lock[1][hash1(x) % lock[1].length].lock();
    }
    public void release(int x) {
        lock[0][hash0(x) % lock[0].length].unlock();
        lock[1][hash1(x) % lock[1].length].unlock();
    }

    public void resize() {
        int oldCapacity = capacity;
        for (ReentrantLock aLock : lock[0]) {
            aLock.lock();
        }
        try {
            if (capacity != oldCapacity) {
                return;
            }
            List<Integer>[][] oldTable = table;
            capacity = 2 * capacity;
            table = (ArrayList<Integer>[][]) new ArrayList[2][capacity];
            for (List<Integer>[] row : table) {
                for (int i = 0; i < row.length; i++) {
                    row[i] = new ArrayList<Integer>(PROBE_SIZE);
                }
            }
            for (List<Integer>[] row : oldTable) {
                for (List<Integer> set : row) {
                    for (int z : set) {
                        add(z);
                    }
                }
            }
        } finally {
            for (ReentrantLock aLock : lock[0]) {
                aLock.unlock();
            }
        }
    }
}
