/**
 * Created by Shengwei_Wang on 11/12/16.
 */
public interface HashTable {
    public boolean add(int key);
    public boolean contains(int key);
    public boolean remove(int key);
//    public void print();
}
