import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Shengwei_Wang on 11/12/16.
 */
public class Hopscotch implements HashTable {

    final static int HOP_RANGE = 32;
    final static int ADD_RANGE = 256;     //cache range
    final static int MAX_SEGMENTS = 1048576; //normally should be a larger number, like 2^30

    class Bucket {
        volatile long _hop_info; //stores hop information,
        volatile int _key;
//        volatile int _data;
        ReentrantLock _lock;
        //CTR - bucket
        Bucket() {
            _hop_info = 0;
            _lock = new ReentrantLock();
            _key = -1;
//            _data = -1;
        }
    }

    Bucket segments_arys[];		// The actual table
    int BUSY;

    public Hopscotch(){
        segments_arys = new Bucket[MAX_SEGMENTS+256];
        for (int i = 0; i < MAX_SEGMENTS+256; i++) {
            segments_arys[i] = new Bucket();
        }
        BUSY = -1;
    }

    int[] find_closer_bucket(int free_bucket_index,int free_distance,int val) {
        //0 - free distance, 1 - val, 2 - new free bucket
        int[] result = new int[3];
        int move_bucket_index = free_bucket_index - (HOP_RANGE-1);
        Bucket move_bucket = segments_arys[move_bucket_index];

        for(int free_dist = (HOP_RANGE -1); free_dist > 0; --free_dist) {
            long start_hop_info = move_bucket._hop_info;
            int move_free_distance = -1;
            long mask = 1;
            for(int i = 0; i < free_dist; ++i, mask <<= 1) {
                if((mask & start_hop_info) >= 1) {
                    move_free_distance = i;
                    break;
                }
            }
			/*When a suitable bucket is found, it's content is moved to the old free_bucket*/
            if(-1 != move_free_distance) {
                move_bucket._lock.lock();
                if(start_hop_info == move_bucket._hop_info) {
                    int new_free_bucket_index = move_bucket_index + move_free_distance;
                    Bucket new_free_bucket = segments_arys[new_free_bucket_index];
					/*Updates move_bucket's hop_info, to indicate the newly inserted bucket*/
                    move_bucket._hop_info |= (1 << free_dist);
//                    segments_arys[free_bucket_index]._data = new_free_bucket._data;
                    segments_arys[free_bucket_index]._key = new_free_bucket._key;

                    new_free_bucket._key = BUSY;
//                    new_free_bucket._data = BUSY;
					/*Updates move_bucket's hop_info, to indicate the deleted bucket*/
                    move_bucket._hop_info &= ~(1<<move_free_distance);

                    free_distance = free_distance - free_dist + move_free_distance;
                    move_bucket._lock.unlock();
                    result[0] = free_distance;
                    result[1] = val;
                    result[2] = new_free_bucket_index;
                    return result;
                }
                move_bucket._lock.unlock();
            }
            ++move_bucket_index;
            move_bucket = segments_arys[move_bucket_index];
        }
        segments_arys[free_bucket_index]._key = -1;
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;
        return result;
    }

    public boolean add(int key) {
        int val = 1;
        int hash = ((key) & (MAX_SEGMENTS - 1));
        Bucket start_bucket = segments_arys[hash];

        start_bucket._lock.lock();
        if (contains(key)) {
            start_bucket._lock.unlock();
            return false;
        }

        int free_bucket_index = hash;
        Bucket free_bucket = segments_arys[hash];
        int free_distance = 0;
        //look for free bucket
        for (; free_distance < ADD_RANGE; ++free_distance) {
            if (-1 == free_bucket._key) {
                free_bucket._key = BUSY;
                break;
            }
            ++free_bucket_index;
            free_bucket = segments_arys[free_bucket_index];
        }
        if (free_distance < ADD_RANGE) {
            do {
                if (free_distance < HOP_RANGE) {
                    start_bucket._hop_info |= (1 << free_distance);
                    free_bucket._key = key;
                    start_bucket._lock.unlock();
                    return true;
                } else {
                    int[] closest_bucket_info = find_closer_bucket(free_bucket_index, free_distance, val);
                    free_distance = closest_bucket_info[0];
                    val = closest_bucket_info[1];
                    free_bucket_index = closest_bucket_info[2];
                    free_bucket = segments_arys[free_bucket_index];
                }
            } while (0 != val);
        }

        //need to resize
        start_bucket._lock.unlock();
        return false;
    }
    public boolean remove(int key) {
        int hash = ((key) & (MAX_SEGMENTS-1));
        Bucket start_bucket = segments_arys[hash];
        // lock the appropriate neighbourhood
        start_bucket._lock.lock();

        long hop_info = start_bucket._hop_info;
        long mask = 1;
        for(int i = 0; i < HOP_RANGE; ++i, mask <<= 1) {
            if((mask & hop_info) >= 1){
                Bucket check_bucket = segments_arys[hash+i];
                if(key == check_bucket._key){
                    check_bucket._key = -1;
//                    check_bucket._data = -1;
                    start_bucket._hop_info &= ~(1<<i);
                    start_bucket._lock.unlock();
                    return true;
                }
            }
        }
        start_bucket._lock.unlock();
        return false;
    }
    public boolean contains(int key) {
        int hash = ((key) & (MAX_SEGMENTS-1));
        Bucket start_bucket = segments_arys[hash];

        long hop_info = start_bucket._hop_info;
        long mask = 1;
        for(int i = 0; i < HOP_RANGE; ++i, mask <<= 1) {
            if((mask & hop_info) >= 1) {
                Bucket check_bucket = segments_arys[hash+i];
                if(key == check_bucket._key) {
                    return true;
                }
            }
        }
        return false;
    }
}
