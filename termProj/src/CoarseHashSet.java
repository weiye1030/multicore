/**
 * Created by Colinhu on 11/13/16.
 */
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CoarseHashSet implements HashTable{
    final Lock lock;
    ArrayList<Integer>[] table;
    int setSize;
    CoarseHashSet(int capacity){
        setSize = 0;
        table = (ArrayList<Integer>[]) new ArrayList[capacity];
        for(int i = 0; i < capacity; i++){
            table[i] = new ArrayList<Integer>();
        }
        lock = new ReentrantLock();
    }

    public boolean contains(int x){
        acquire(x);
        try {
            int myBucket = Math.abs(x % table.length);
            return table[myBucket].contains((Integer)x);
        }finally {
            release(x);
        }
    }

    public boolean add(int x){
        boolean result = false;
        acquire(x);
        try{
            int myBucket = Math.abs(x % table.length);
            if(!table[myBucket].contains((Integer)x)){
                table[myBucket].add((Integer)x);
                result = true;
                setSize++;
            }
            //result = table[myBucket].add(x);
            //setSize = result ? setSize + 1 : setSize;
        }finally {
            release(x);
        }
        if(policy())
            resize();
        return result;
    }
    public boolean remove(int x){
        acquire(x);
        try {
            int myBucket = Math.abs(x % table.length);
            boolean result = table[myBucket].remove((Integer)x);
            setSize = result ? setSize - 1 : setSize;
            return result;
        } finally {
            release(x);
        }
    }

    public final void acquire(int x){
        lock.lock();
    }
    public void release(int x){
        lock.unlock();
    }

    public boolean policy(){
        if(setSize / table.length > 4) System.out.println("resize once");
        return setSize / table.length > 4;
    }

    public void resize() {
        int oldCapacity = table.length;
        lock.lock();
        try {
            if (oldCapacity != table.length) {
                return;
            }
            int newCapacity = 2 * oldCapacity;
            ArrayList<Integer>[] oldTable = table;
            table = (ArrayList<Integer>[]) new ArrayList[newCapacity];
            for (int i = 0; i < newCapacity; i++) {
                table[i] = new ArrayList<Integer>();
            }
            for (List<Integer> bucket : oldTable) {
                for (Integer x : bucket) {
                    table[Math.abs(x % table.length)].add(x);
                }
            }
        } finally {
            lock.unlock();
        }
    }
}
