import java.util.*; // For AbstractMap, AbstractSet, Arrays

public class Cuckoo_maplock implements HashTable {
	CuckooHashMap<Integer, Integer> map;
	public Cuckoo_maplock (int capacity) {
		map = new CuckooHashMap<>();
	}
	public boolean add (int key) {
		return map.put((Integer)key, (Integer)key) == null;
	}
	public boolean remove(int key) {
		return map.remove((Integer)key) != null;
	}

	public boolean contains(int key) {
		return map.containsKey((Integer) key);
	}
}