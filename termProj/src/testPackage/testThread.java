package testPackage;

/**
 * Created by Shengwei_Wang on 11/14/16.
 */
public class testThread {

        public static void main(String[] argv) {
            TestThread_nojunit.parallelAdd(4);
        }

    }

class TestThread_nojunit extends Thread {
    static Counter ct;
    public static void parallelAdd (int numThreads) {
        ct = new Counter();
        TestThread_nojunit[] t = new TestThread_nojunit[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new TestThread_nojunit(i);
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(ct.counter);
    }

    int myId;
    public TestThread_nojunit(int id) {
        this.myId = id;
    }
    public void run() {
        for (int i = 0; i < 10000000; ++i)
            ct.addition();
    }
}
