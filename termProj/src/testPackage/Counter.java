package testPackage;

import org.deuce.Atomic;

/**
 * Created by Shengwei_Wang on 11/14/16.
 */
public class Counter {
    int counter;
    public Counter (){
        counter = 0;
    }
    @Atomic
    public void addition() {
        ++counter;
    }
}
