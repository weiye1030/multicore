import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Shengwei_Wang on 11/12/16.
 */
public class CuckooTest {

    @Test
    public void testRandom() throws Exception {
        Random random = new Random();
        HashTable table = new Cuckoo();
        int n = 20;
        int[] inputList = new int[n];
        int[] testList = new int[n + 3];
        boolean check[] = new boolean[n + 3];
        for (int i = 0; i < n; ++i) {
            inputList[i] = random.nextInt(10000);
            testList[i] = inputList[i];
            check[i] = true;
        }
        testList[n] = 10001;
        testList[n + 1] = 10002;
        testList[n + 2] = 10003;
        for (int i = 0; i < inputList.length; ++i) {
            table.add(inputList[i]);
        }
        for (int i = 0; i < testList.length; ++i) {
            assert (check[i] == table.contains(testList[i]));
        }
    }

    @Test
    public void testExplicit() throws Exception {
        HashTable table = new Cuckoo();
        int[] inputList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312};
        int[] testList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312, 1001,1002,1003};
        for (int i = 0; i < inputList.length; ++i) {
            table.add(inputList[i]);
        }
        int i = 0;
        for (; i < inputList.length; ++i) {
            assert (table.contains(testList[i]));
        }
        for (;i < testList.length; ++i)
            assert (!table.contains(testList[i]));
    }

    @Test
    public void testParallel() throws Exception {
       MyTestThread.parallelAdd(4);

    }

}

class MyTestThread extends Thread {
    static HashTable table;
    public static void parallelAdd (int numThreads) {
        table = new Cuckoo();
        MyTestThread[] t = new MyTestThread[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new MyTestThread(i);
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    int myId;
    public MyTestThread(int id) {
        this.myId = id;
    }
    public void run() {
        int[] inputList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312};
        int[] testList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312, 1001,1002,1003};
        for (int k : inputList)
            System.out.println("add id: " + myId + "  succ: " + table.add(k) + "  num :" + k);
        int i = 0;
        for (; i < inputList.length; ++i) {
            assert (table.contains(testList[i]));
        }
        for (; i < testList.length; ++i)
            assert (!table.contains(testList[i]));
    }
}
