//import org.deuce.Atomic;

//import testPackage.Counter;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Shengwei_Wang on 11/14/16.
 */
public class HashTableTest {
    public void testRandom(HashTable table) throws Exception {
        Random random = new Random();
        int n = 20;
        int[] inputList = new int[n];
        int[] testList = new int[n + 3];
        boolean check[] = new boolean[n + 3];
        for (int i = 0; i < n; ++i) {
            inputList[i] = random.nextInt(10000);
            testList[i] = inputList[i];
            check[i] = true;
        }
        testList[n] = 10001;
        testList[n + 1] = 10002;
        testList[n + 2] = 10003;
        for (int i = 0; i < inputList.length; ++i) {
            table.add(inputList[i]);
        }

        for (int i = 0; i < testList.length; ++i) {
            assert (check[i] == table.contains(testList[i]));
        }
    }

    public void testExplicit(HashTable table) throws Exception {
        int n = 16;
        int invalid = 3;
        int[] inputList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312};
        int[] testList = {1,1025,3,5,8,223,434,543,293239823,38343787,4737373,38471231,382382,1312,84838482,12312312, 1001,1002,1003};
        for (int i = 0; i < n; ++i) {
            table.add(inputList[i]);
        }
        int i = 0;
        for (; i < n; ++i) {
            assert (table.contains(testList[i]));

        }
        for (;i < n + invalid; ++i)
            assert (!table.contains(testList[i]));
        for (i = 0; i < n;++i) {
//            System.out.println("XXX");
            assert (table.remove(inputList[i]));
        }
        for (i = 0; i < n; ++i) {
            assert (!table.contains(testList[i]));
        }
    }


    public void testParallel(HashTable table) throws Exception {
        TestThread.parallelAdd(2, table, 40000);
    }
    public static void main(String[] argv) {
        HashTableTest testcast = new HashTableTest();
        int defaultCapacity = 1048576;
        try {

            testcast.testExplicit(new Cuckoo_maplock(defaultCapacity));
            testcast.testParallel(new Cuckoo_maplock(defaultCapacity));
//            testcast.testRandom(new Hopscotch_waitfree());
//            testcast.testExplicit(new Hopscotch_waitfree());
//            testcast.testParallel(new Hopscotch_waitfree());

//            testcast.testRandom(new Cuckoo());
//            testcast.testExplicit(new Cuckoo());
//            testcast.testParallel(new Cuckoo(defaultCapacity));
//
//            testcast.testRandom(new Cuckoo_lockfree(32768));
//            testcast.testExplicit(new Cuckoo_lockfree(32768));
//            testcast.testParallel(new Cuckoo_lockfree(32768));
          

//            testcast.testRandom(new LockFreeHashSet(32768));
//            testcast.testExplicit(new LockFreeHashSet(32768));
//            testcast.testParallel(new LockFreeHashSet(32768));


//            //can pass
//            testcast.testRandom(new Hopscotch());
//            testcast.testExplicit(new Hopscotch());
//            testcast.testParallel(new Hopscotch());
//
//            testcast.testRandom(new Hopscotch_waitfree());
//            testcast.testExplicit(new Hopscotch_waitfree());
//            testcast.testParallel(new Hopscotch_waitfree());

          //  testcast.testRandom(new Cuckoo());
////            System.out.println("first");
         //   testcast.testExplicit(new Cuckoo());
////            System.out.println("second");
         //   testcast.testParallel(new Cuckoo());


//            //has problem
//            testcast.testRandom(new LockFreeHashSet(defaultCapacity));
//            testcast.testExplicit(new LockFreeHashSet(defaultCapacity));
//            testcast.testParallel(new LockFreeHashSet(defaultCapacity));


//            //can pass
//            testcast.testRandom(new StripedHashSet(defaultCapacity));
//            testcast.testExplicit(new StripedHashSet(defaultCapacity));
//            testcast.testParallel(new StripedHashSet(defaultCapacity));
//
//            testcast.testRandom(new CoarseHashSet(defaultCapacity));
//            testcast.testExplicit(new CoarseHashSet(defaultCapacity));
//            testcast.testParallel(new CoarseHashSet(defaultCapacity));


        } catch  (Exception e) {
//            System.out.println("~~~");
            System.err.println(e);
        }
    }

}

class TestThread extends Thread {
    static HashTable table;
    static int[] inputList;
    static AtomicInteger counter = new AtomicInteger(0);
    static AtomicInteger hitCounter = new AtomicInteger(0);
    static AtomicInteger removeCounter = new AtomicInteger(0);
    static AtomicInteger secondCounter = new AtomicInteger(0);
    static CyclicBarrier barrier;
    static int n; // test scale
    public static void parallelAdd (int numThreads, HashTable table, int n) {
//        table = new Hopscotch();
        counter = new AtomicInteger(0);
        hitCounter = new AtomicInteger(0);
        removeCounter = new AtomicInteger(0);
        secondCounter = new AtomicInteger(0);
        barrier = new CyclicBarrier(numThreads);
        Random r = new Random();
        TestThread.table = table;
        TestThread.n = n;
        inputList = new int[n];
        Set<Integer> hashset = new HashSet<>();
        for (int i = 0; i < n; ++i) {
//<<<<<<< HEAD
         inputList[i] = i+1; //r.nextInt();
//=======
           // inputList[i] = r.nextInt(Integer.MAX_VALUE);
//>>>>>>> eae8bd565ade3ed75e1c0f50a18442d708ae0705
            if (hashset.contains(inputList[i])) {
                --i;
            } else {
                hashset.add(inputList[i]);
            }
        }
        TestThread[] t = new TestThread[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new TestThread(i);
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(counter);
        System.out.println(hitCounter);
        System.out.println(removeCounter);
        System.out.println(secondCounter);
        System.out.println("Test finished!");
//        assert counter.get() == n;
//        assert hitCounter.get() == 4 * n;
//        assert removeCounter.get() == n / 2;
//        assert secondCounter.get() == 2 * n;
//        assert 1 == 2;
    }

    int myId;
    public TestThread(int id) {
        this.myId = id;
    }
    public void run() {
        int i = 0;
        for (int k : inputList) {
            if (table.add(k))
                counter.getAndIncrement();
        }

        for (i = 0; i < n; ++i) {
            if (table.contains(inputList[i]))
                hitCounter.getAndIncrement();

        }
        try {
            barrier.await();
        } catch (Exception e) {
            System.err.println("barrier error !");
        }
        for (i = 0; i < n / 2; ++i) {
            if (table.remove(inputList[i]))
                removeCounter.getAndIncrement();
        }
        for (i = 0; i < n; ++i)
            if (table.contains(inputList[i]))
                secondCounter.getAndIncrement();
    }
}
