/**
 * Created by Shengwei_Wang on 11/16/16.
 */
public interface HashFunction<T> {
    /**
     * Given an object, returns the hash code of that object.
     *
     * @param obj The object whose hash code should be computed.
     * @return The object's hash code.
     */
    public int hash(T obj);
}
