import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Shengwei_Wang on 11/15/16.
 */
public class Evaluation {
    final static int n = 400000;
    final static int threadLimit = 8;
    final static int repeatTime = 10;
    final static int defaultCapacity = 1048576;
    public static void main(String[] strings) throws IOException{

        File file = new File("output" + n + ".txt");
        FileWriter fw = new FileWriter(file);
        String[] hashName = {"StripedHashSet", "CoarseHashSet", "LockFreeHashSet", "Cuckoo", "Cuckoo_lockfree", "Hop_lockbase", "Hop_lockfree"};
        final int[] rate = {0, 3, 5, 9};
        int[][][] res = new int[7][threadLimit + 1][rate.length]; // 7 hashtable, 32 threads, rate.length test mod

        //rate == 5 half half, rate == 0 100% insertion, rate == 9 10% insertion


        for (int times = 0; times < repeatTime; ++times) {
            System.out.println(times);
            // create InputList
            Random r = new Random();
            int[] inputList = new int[n];
            boolean[][] operationList = new boolean[rate.length][n]; //f - add, t - contains
            int[] defaultInput = new int[defaultCapacity * 4 /10];
            for (int i = 0; i < defaultCapacity * 4 / 10; ++i)
                defaultInput[i] = r.nextInt(Integer.MAX_VALUE);
            for (int i = 0; i < n; ++i) {
                inputList[i] = r.nextInt(Integer.MAX_VALUE);
                for (int j = 0; j < rate.length; ++j) {
                    if (r.nextInt(10) < rate[j])
                        operationList[j][i] = true;
                }
            }


//            System.out.println("n = " + n + "     default capacity = " + defaultCapacity);
            fw.write("n = " + n + "     default capacity = " + defaultCapacity + "\n");

            for (int i = 1; i <= threadLimit; i *= 2) {
//                System.out.println(i);
                for (int j = 0; j < rate.length; ++j) {
                    res[0][i][j] += PerformanceTestThread.parallelAdd(i, new StripedHashSet(defaultCapacity), n, inputList, operationList[j], defaultInput);
                    res[1][i][j] += PerformanceTestThread.parallelAdd(i, new CoarseHashSet(defaultCapacity), n, inputList, operationList[j], defaultInput);
                    res[2][i][j] += PerformanceTestThread.parallelAdd(i, new LockFreeHashSet(defaultCapacity), n, inputList, operationList[j], defaultInput);
                    res[3][i][j] += PerformanceTestThread.parallelAdd(i, new Cuckoo(defaultCapacity), n, inputList, operationList[j], defaultInput);
                    res[4][i][j] += 1;
//                    res[4][i][j] += PerformanceTestThread.parallelAdd(i, new Cuckoo_lockfree(defaultCapacity), n, inputList, operationList[j], defaultInput);
                    res[5][i][j] += PerformanceTestThread.parallelAdd(i, new Hopscotch(), n, inputList, operationList[j], defaultInput);
                    res[6][i][j] += PerformanceTestThread.parallelAdd(i, new Hopscotch_waitfree(), n, inputList, operationList[j], defaultInput);
                }
            }
        } //times

        for (int k = 0; k < rate.length; ++k) {
            fw.write("\n" + 10 * (10 - rate[k]) + "% ");
            for (int i = 0; i < 7; ++i) {
                fw.write(hashName[i] + " ");
            }
            for (int thread = 1; thread <= threadLimit; thread*=2) {
                fw.write("\n");
                fw.write(thread + " ");
                for (int i = 0; i < 7; ++i) {
                    fw.write(n / res[i][thread][k] + " ");
                }
            }
        }
        fw.close();
    }
}


class PerformanceTestThread extends Thread {
    static HashTable table;
    static int n; // test scale
    static int rate;
    static int[] inputList;
    static boolean[] operationList;
    public static long parallelAdd (int numThreads, HashTable table, int n, int[] inputList, boolean[] operationList, int[] defaultinput) {
        PerformanceTestThread.table = table;
        PerformanceTestThread.n = n / numThreads;
//        PerformanceTestThread.rate = rate;
        PerformanceTestThread[] t = new PerformanceTestThread[numThreads];
        PerformanceTestThread.inputList = inputList;
        PerformanceTestThread.operationList = operationList;
        for (int i = 0; i < defaultinput.length; ++i)
            table.add(defaultinput[i]);
        long start = System.currentTimeMillis();
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PerformanceTestThread(i);
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        long end = System.currentTimeMillis();
        return end - start;
    }

    int myId;
    public PerformanceTestThread(int id) {
        this.myId = id;
    }
    public void run() {
        for (int i = 0; i < n; ++i) {
            if (operationList[i + Evaluation.n / n * myId])
                table.contains(inputList[i + Evaluation.n / n * myId]);
            else
                table.add(inputList[i + Evaluation.n / n * myId]);
        }
    }
}
