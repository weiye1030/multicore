/**
 * Created by Colinhu on 11/14/16.
 */
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicMarkableReference;

public class LockFreeHashSet implements HashTable {
    BucketList[] bucket;
    AtomicInteger bucketSize;
    AtomicInteger setSize;
    private static final double THRESHOLD = 4.0;

    public LockFreeHashSet(int capacity) {
        bucket = (BucketList[]) new BucketList[capacity];
        bucket[0] = new BucketList();
        bucketSize = new AtomicInteger(32768); //32768 -> 1
        setSize = new AtomicInteger(0);
    }
    
    public void print(){
        System.out.println("setsize" + setSize.get());
    }

    public boolean add(int x) {
        int myBucket = Math.abs(BucketList.hashCode(x) % bucketSize.get());
        BucketList b = getBucketList(myBucket);
        if (!b.add(x))
            return false;
        int setSizeNow = setSize.getAndIncrement();
        int bucketSizeNow = bucketSize.get();
        if (setSizeNow / (double)bucketSizeNow > THRESHOLD)
            bucketSize.compareAndSet(bucketSizeNow, 2 * bucketSizeNow);
        //System.out.println("add: " + x);
        return true;
    }
    public boolean remove(int x) {
        int myBucket = Math.abs(BucketList.hashCode(x) % bucketSize.get());
        BucketList b = getBucketList(myBucket);
        if (!b.remove(x)) {
            return false;		// she's not there
        }
        return true;
    }
    public boolean contains(int x) {
        int myBucket = Math.abs(BucketList.hashCode(x) % bucketSize.get());
        //System.out.println("bucketsize: " + bucketSize);
        BucketList b = getBucketList(myBucket);
        //System.out.println("contain: " + x);
        return b.contains(x);
    }
    private BucketList getBucketList(int myBucket) {
        if (bucket[myBucket] == null)
            initializeBucket(myBucket);
        return bucket[myBucket];
    }
    private void initializeBucket(int myBucket) {
        int parent = getParent(myBucket);
        if (bucket[parent] == null)
            initializeBucket(parent);
        BucketList b = bucket[parent].getSentinel(myBucket);
        if (b != null)
            bucket[myBucket] = b;
    }
    private int getParent(int myBucket){
        int parent = bucketSize.get();
        do {
            parent = parent >> 1;
        } while (parent > myBucket);
        parent = myBucket - parent;
        return parent;
    }
}

class BucketList {
    static final int WORD_SIZE = 24;
    static final int LO_MASK = 0x00000001;
    static final int HI_MASK = 0x00800000;
    static final int MASK = 0x00FFFFFF;
    Node head;
    public BucketList(){
        this.head = new Node(0);
        this.head.next = new AtomicMarkableReference<Node>(new Node(Integer.MAX_VALUE), false);
    }
    private BucketList(Node e){
        this.head = e;
    }

    public static int hashCode(int x){
        return x & MASK;
    }

    public boolean add(int x){
        int key = makeOrdinaryKey(x);
        boolean splice;
        while(true){
            Window window = find(head, key);
            Node pred = window.pred;
            Node curr = window.curr;
            if(curr.key == key){
                return false;
            }
            else{
                Node entry = new Node(key, x);
                entry.next.set(curr, false);
                splice = pred.next.compareAndSet(curr, entry, false, false);
                if(splice)
                    return true;
                else
                    continue;
            }
        }
    }
    public boolean remove(int x) {
        int key = makeOrdinaryKey(x);
        boolean snip;
        while (true) {
            // find predecessor and current entries
            Window window = find(head, key);
            Node pred = window.pred;
            Node curr = window.curr;
            // is the key present?
            if (curr.key != key) {
                return false;
            } else {
                // snip out matching entry
                snip = pred.next.attemptMark(curr, true);
                if (snip)
                    return true;
                else
                    continue;
            }
        }
    }

    public boolean contains(int x) {
        int key = makeOrdinaryKey(x);
        Window window = find(head, key);
        Node pred = window.pred;
        Node curr = window.curr;
        return (curr.key == key);
    }


    public BucketList getSentinel(int index) {
        int key = makeSentinelKey(index);
        boolean splice;
        while (true) {
            // find predecessor and current entries
            Window window = find(head, key);
            Node pred = window.pred;
            Node curr = window.curr;
            // is the key present?
            if (curr.key == key) {
                return new BucketList(curr);
            } else {
                // splice in new entry
                Node entry = new Node(key);
                entry.next.set(pred.next.getReference(), false);
                splice = pred.next.compareAndSet(curr, entry, false, false);
                if (splice)
                    return new BucketList(entry);
                else
                    continue;
            }
        }
    }

    public static int reverse(int key) {
        int loMask = LO_MASK;
        int hiMask = HI_MASK;
        int result = 0;
        for (int i = 0; i < WORD_SIZE; i++) {
            if ((key & loMask) != 0) {  // bit set
                result |= hiMask;
            }
            loMask <<= 1;
            hiMask >>>= 1;  // fill with 0 from left
        }
        return result;
    }

    public int makeOrdinaryKey(int x){
        int code = x & MASK;
        return reverse(code | HI_MASK);
    }

    public static int makeSentinelKey(int key){
        return reverse(key & MASK);
    }

    private class Node {
        int key;
        int value;
        AtomicMarkableReference<Node> next;
        Node(int key, int object) {      // usual constructor
            this.key   = key;
            this.value = object;
            this.next  = new AtomicMarkableReference<Node>(null, false);
        }
        Node(int key) { // sentinel constructor
            this.key  = key;
            this.next = new AtomicMarkableReference<Node>(null, false);
        }

        Node getNext() {
            boolean[] cMarked = {false}; // is curr marked?
            boolean[] sMarked = {false}; // is succ marked?
            Node entry = this.next.get(cMarked);
            while (cMarked[0]) {
                Node succ = entry.next.get(sMarked);
                this.next.compareAndSet(entry, succ, true, sMarked[0]);
                entry = this.next.get(cMarked);
            }
            return entry;
        }
    }

    class Window{
        public Node pred;
        public Node curr;
        Window(Node pred, Node curr){
            this.pred = pred;
            this.curr = curr;
        }
    }
    public Window find(Node head, int key) {
        Node pred = head;
        Node curr = head.getNext();
        while (curr.key < key) {
            pred = curr;
            curr = pred.getNext();
        }
        return new Window(pred, curr);
    }
}