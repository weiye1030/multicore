import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;
import java.util.*;
/**
 * Created by Colinhu on 11/13/16.
 */
public class StripedHashSet implements HashTable {
    final ReentrantLock[] locks;
    ArrayList<Integer>[] table;
    int setSize;
    public StripedHashSet(int capacity){
        setSize = 0;
        table = (ArrayList<Integer>[]) new ArrayList[capacity];
        for(int i = 0; i < capacity; i++){
            table[i] = new ArrayList<Integer>();
        }

        locks = new ReentrantLock[capacity];
        for(int i = 0; i < locks.length; i++){
            locks[i] = new ReentrantLock();
        }
    }

    public final void acquire(int x){
        locks[Math.abs(x % locks.length)].lock();
    }
    public void release(int x){
        locks[Math.abs(x % locks.length)].unlock();
    }

    public boolean contains(int x){
        acquire(x);
        try {
            int myBucket = Math.abs(x % table.length);
            return table[myBucket].contains((Integer)x);
        }finally {
            release(x);
        }
    }

    public boolean add(int x){
        boolean result = false;
        acquire(x);
        try{
            int myBucket = Math.abs(x % table.length);
            if(!table[myBucket].contains((Integer)x)){
                table[myBucket].add((Integer)x);
                result = true;
                setSize++;
            }
            //result = table[myBucket].add(x);
            //setSize = result ? setSize + 1 : setSize;
        }finally {
            release(x);
        }
        if(policy())
            resize();
        return result;
    }
    public boolean remove(int x){
        acquire(x);
        try {
            int myBucket = Math.abs(x % table.length);
            boolean result = table[myBucket].remove((Integer)x);
            setSize = result ? setSize - 1 : setSize;
            return result;
        } finally {
            release(x);
        }
    }


    public boolean policy(){
        if(setSize / table.length > 4) System.out.println("resize once");
        return setSize / table.length > 4;
    }

    public void resize(){
        int oldCapacity = table.length;
        for(Lock lock : locks){
            lock.lock();
        }
        try{
            if(oldCapacity != table.length)
                return;
            int newCapacity = 2 * oldCapacity;
            ArrayList<Integer>[] oldTable = table;
            table = (ArrayList<Integer>[]) new ArrayList[newCapacity];
            for (int i = 0; i < newCapacity; i++) {
                table[i] = new ArrayList<Integer>();
            }
            for (List<Integer> bucket : oldTable) {
                for (Integer x : bucket) {
                    table[Math.abs(x % table.length)].add(x);
                }
            }
        }finally {
            for(Lock lock : locks){
                lock.unlock();
            }
        }
    }
}
