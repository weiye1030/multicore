public class TestIncrement {
    public static void main(String[] args) {
        int c = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        int result = PIncrement.parallelIncrement(c, n);
        System.out.println(result);
    }
}
