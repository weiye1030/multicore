public class PIncrement extends Thread {
    static int c;
    int increment;
    int myId;
    Lock lock;
    public PIncrement(int i, int k, Lock lock) {
        myId = i;
        increment = k;
        this.lock = lock;
    }
    public void run() {
        for (int k = 0; k < increment; k++) {
            lock.requestCS(myId);
            ++c;
            lock.releaseCS(myId);
        }
    }

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement.c = c;
        PIncrement t[] = new PIncrement[numThreads];
        Lock lock = new Bakery(numThreads);
        //Lock lock = new FastMutex(numThreads);
        for (int i = 0; i < numThreads; i++) {
            int increment = 1200000/numThreads;
            t[i] = new PIncrement(i, increment, lock);
            t[i].start();
        }
        try {
            for (int i = 0; i < numThreads; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {};
        
        return PIncrement.c;
    }
}
