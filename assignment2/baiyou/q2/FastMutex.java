import java.util.concurrent.atomic.AtomicInteger;

public class FastMutex implements Lock {
    volatile int X, Y;
    int N;
	AtomicInteger[] flag;
	public FastMutex(int numProc) {
        N = numProc;
        X = Y = -1;
		flag = new AtomicInteger[N];
		for (int j = 0; j < N; j++) {
			flag[j] = new AtomicInteger();
        }
	}

	public void requestCS(int i) {
        while (true) {
            flag[i].set(1);
            X = i;
            if (Y != -1) {  // Left
                flag[i].set(0);
                while(Y != -1) ;
                continue;
            } else {
                Y = i;
                if (X == i)
                    return;
                else {
                    flag[i].set(0);
                    for (int j = 0; j < N; j++) {
                        while(flag[j].get() != 0) ;
                    }
                    if (Y == i)
                        return;
                    else {
                        while (Y != -1) ;
                        continue;
                    }
                }
            }
        }
    }

    public void releaseCS(int i) {
        Y = -1;
        flag[i].set(0);
    }
}
