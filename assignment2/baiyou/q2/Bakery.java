import java.util.concurrent.atomic.*;

public class Bakery implements Lock {
    int N;
    AtomicBoolean[] choosing;
	AtomicInteger[] number;
	public Bakery(int numProc) {
        N = numProc;
        choosing = new AtomicBoolean[N];
		number = new AtomicInteger[N];
		for (int j = 0; j < N; j++) {
            choosing[j] = new AtomicBoolean();
			number[j] = new AtomicInteger();
        }
	}

	public void requestCS(int i) {
        choosing[i].set(true);
        for (int j = 0; j < N; j++) {
            int ni = number[i].get();
            int nj = number[j].get();
            if (nj > ni) number[i].set(nj);
        }
        number[i].getAndIncrement();
        choosing[i].set(false);

        for (int j = 0; j < N; j++) {
            while (choosing[j].get()) ;
            while (true) {
                int ni = number[i].get();
                int nj = number[j].get();
                //if ((nj == 0) || !((nj < ni) || (ni == nj && j < i))) break;
                if ((nj == 0) || ((ni < nj) || (ni == nj && i <= j))) break;
            }
        }
    }

    public void releaseCS(int i) {
        number[i].set(0);
    }
}
