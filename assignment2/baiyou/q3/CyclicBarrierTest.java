/**
 * Main class
 */
public class CyclicBarrierTest {
    public static void main(String[] args) {
           /*
            * Create CountDownLatch with 3 parties, when all 3 parties
            * will reach common barrier point CyclicBarrierEvent will be
            * triggered i.e. run() method of CyclicBarrierEvent will be called
            */
           CyclicBarrier cyclicBarrierCustom=new CyclicBarrier(3);
           System.out.println("CountDownLatch has been created with parties=3,"
                        + " when all 3 parties will reach common barrier point "
                        + "CyclicBarrierEvent will be triggered");
 
           MyRunnable myRunnable1=new MyRunnable(cyclicBarrierCustom);
           
           //Create and start 3 threads
           new Thread(myRunnable1,"Thread-1").start();
           new Thread(myRunnable1,"Thread-2").start();
           new Thread(myRunnable1,"Thread-3").start();

           try {
               Thread.sleep(5000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }

           //Create and start 3 more threads
           new Thread(myRunnable1,"Thread-4").start();
           new Thread(myRunnable1,"Thread-5").start();
           new Thread(myRunnable1,"Thread-6").start();
    
           
           
    }
 
}
 




class MyRunnable implements Runnable{
 
    CyclicBarrier cyclicBarrierCustom;
    
    MyRunnable(CyclicBarrier cyclicBarrierCustom){
           this.cyclicBarrierCustom=cyclicBarrierCustom;
    }
    CyclicBarrierEvent cyclicBarrierEvent = new CyclicBarrierEvent();
    
    @Override
    public void run() {
           
           System.out.println(Thread.currentThread().getName() +
                        " is waiting for all other threads to reach common barrier point");
 
           int id = -1;
           try {
                  Thread.sleep(1000);
                  /*
                   * when all 3 parties will call await() method (i.e. common barrier point)
                   * CyclicBarrierEvent will be triggered and all waiting threads will
                   * be released.
                   */
 
                  id = cyclicBarrierCustom.await();
                  if (id == 0) cyclicBarrierEvent.run();
           } catch (InterruptedException e) {
                  e.printStackTrace();
           }
           System.out.println("As all threads have reached common barrier point "
                        + Thread.currentThread().getName() + " return (" + id +
                        ") has been released");
    }
    
}
 
 


class CyclicBarrierEvent implements Runnable{
 
    public void run() {
           System.out.println("As all threads have reached common barrier point "
                        + ", CyclicBarrierEvent has been triggered");
    }
    
}
