public class CyclicBarrier {
    volatile int value, initValue;
    volatile boolean ok;

    public CyclicBarrier(int parties) {
        initValue = value = parties;
        ok = false;
    }

    public int await() throws InterruptedException {
        P();
        int res = value;
        V();
        //System.out.println("value in res is " + res);
        return res;
    }

    public synchronized void P() {
        value--;
        if (value == 0) {
            ok = true;
        }
        //System.out.println("value in P is from " + (value + 1) + " to "+ value);
        while (!ok) Util.myWait(this);
    }

    public synchronized void V() {
        //System.out.println("value in V is from " + value);
        notify();
        value++;
        //System.out.println("value in V is to " + value);
        if (value == initValue) ok = false;
    }
}
