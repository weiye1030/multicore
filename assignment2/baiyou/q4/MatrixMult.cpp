#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <omp.h>
struct Matrix {
	double **A;
	int nRows;
	int nCols;
};

Matrix* readMatrix(std::string const& filename) {
	FILE *fp = fopen(filename.c_str(), "r");
	
	Matrix *mat = (Matrix *)malloc(sizeof(Matrix));
	fscanf(fp, "%ld%ld", &mat->nRows, &mat->nCols);
	mat->A = (double**)malloc(mat->nRows * sizeof(double*));
    
    #pragma omp parallel for schedule (static)
	for (int i = 0; i < mat->nRows; i++)
		mat->A[i] = (double*)malloc(mat->nCols*sizeof(double));

	for (int i = 0; i < mat->nRows; i++)
		for (int j = 0; j < mat->nCols; j++)
			fscanf(fp,"%lf",&mat->A[i][j]);

	fclose(fp);
	return mat;
}

void writeMatrix(Matrix *mat,std::string const& filename) {
	FILE *fp = fopen(filename.c_str(), "w");
	fprintf(fp, "%ld %ld\n", mat->nRows, mat->nCols);
	for (int i = 0; i < mat->nRows; i++) {
		for(int j = 0; j < mat->nCols; j++)
			fprintf(fp,"%lf ",mat->A[i][j]);
		fprintf(fp,"\n");
	}

	fclose(fp);
}

Matrix* multiplyMatrix(Matrix* a, Matrix* b, int nthread) {
    Matrix *mat;
    mat = (Matrix *)malloc(sizeof(Matrix));
    mat->nRows = a->nRows;
    mat->nCols = b->nCols;
    mat->A = (double**)malloc(mat->nRows * sizeof(double*));

    #pragma omp parallel for schedule (static)
    for (int i = 0; i < mat->nRows; i++) {
        mat->A[i] = (double*)malloc(mat->nCols * sizeof(double));
    }

#pragma omp parallel num_threads(nthread)
    {
    #pragma omp for schedule (static)
    for (int i = 0; i < mat->nRows; i++) {
        for (int j = 0; j < mat->nCols; j++)
            mat->A[i][j] = 0;
    }
    
    #pragma omp for schedule (static)
    for (int i = 0; i < mat->nRows; i++) {
        for (int j = 0; j<mat->nCols; j++)
            for (int k = 0; k < a->nCols; k++) {
                mat->A[i][j] += a->A[i][k] * b->A[k][j];
            }
    }

    }
    return mat;
}

int main(int argc, char *argv[]) {
	Matrix* m1 = readMatrix(argv[1]);
	Matrix* m2 = readMatrix(argv[2]);
    int nthread = atoi(argv[3]);
    Matrix* m3 = multiplyMatrix(m1, m2, nthread);
	writeMatrix(m3, "solution.txt");
}
