package q2;

// TODO
// Implement the bakery algorithm

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class BakeryLock implements MyLock {
    int N;
    AtomicBoolean[] choosing;
    AtomicInteger[] number;

    public BakeryLock(int numThread) {
        N = numThread;
        choosing = new AtomicBoolean[N];
        number = new AtomicInteger[N];
        for(int i = 0; i < N; ++i){
            choosing[i] = new AtomicBoolean();
            number[i] = new AtomicInteger();
        }
    }

    @Override
    public void lock(int myId) {
        choosing[myId].set(true);
        for (int j = 0; j < N; ++j){
            int ni = number[myId].get();
            int nj = number[j].get();
            if (nj > ni){
                number[myId].set(nj);
            }
        }
        number[myId].getAndIncrement();
        choosing[myId].set(false);

        for (int j = 0; j < N; ++j) {
            while (choosing[j].get()) ;
            while (true) {
                int ni = number[myId].get();
                int nj = number[j].get();

                if(! (nj != 0 && (nj < ni || (nj == ni && j < myId))))
                    break;
            }
        }
    }

    @Override
    public void unlock(int myId) {
        number[myId].set(0);
    }
}
