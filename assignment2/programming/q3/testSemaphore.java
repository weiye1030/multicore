package q3;

import trash.CountingSemaphore;

/**
 * Created by Shengwei_Wang on 9/20/16.
 */

class TestThreads extends Thread{

    public static void threadCreate(int c, int numThreads){
        TestThreads[] t = new TestThreads[numThreads];
        CyclicBarrier cb = new CyclicBarrier(numThreads);
        for(int i = 0; i < numThreads; ++i){
            t[i] = new TestThreads(i, cb); //set thread id = i
            t[i].start();
        }
        for(int i = 0; i < numThreads; ++i){
            try{
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    int myId;
    CyclicBarrier cb;
    public TestThreads(int id, CyclicBarrier cb){
        myId = id;
        this.cb = cb;
    }
    public void run() {
        for(int i = 0; i < 10; ++i){  //each thread request into cs 10 times
            int k = 9;
            try {
                k = cb.await();      //each thread get a unique number to indicate the order it
                                     //made a request to enter into cs
            } catch (Exception e){

            }
            System.out.println("Thread is : " + myId + " order = " + k);
        }
    }

}

public class testSemaphore {
    public static void main(String[] args) throws Exception {
        long start;
        long end;
        int n = 1;
        int c = 0;
        while(n <= 8){

            System.out.println(n + " Threads: ");
            System.out.println("Test for CyclicBarrier: ");
            start = System.currentTimeMillis();
            TestThreads.threadCreate(c, n);
            end = System.currentTimeMillis();
            System.out.println("part a finished Time: " + (end - start));


            n *=2;

        }
    }
}