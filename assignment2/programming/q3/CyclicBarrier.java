package q3;

/**
 * Created by Shengwei_Wang on 9/20/16.
 */
public class CyclicBarrier {
    volatile int left;
    volatile int next;
    volatile boolean canpass;
    int n;
    public CyclicBarrier(int n){
        this.n = n;
        left = n;
        next = 0;
        canpass = false;
    }
    public int await() throws InterruptedException{

        int res = --left;

        cP();
        canpass = false;
        System.out.println("leave " + res);
        ++next;
        if(next == n) {
            next = 0;
            canpass = true;
        }
        cV();

        return res;
    }
    public synchronized void cP(){
        while(left != 0) {
            Util.myWait(this);
        }
    }
    public synchronized void cV(){
        notify();
        while(!canpass) {
            Util.myWait(this);
        }
        if(left == 0)
            left = n;
        notify();
    }
}
