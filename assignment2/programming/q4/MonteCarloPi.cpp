/*************************************************************************
    > File Name: MonteCarloPi.cpp
    > Author: Wei Ye
    > Mail: yeweizju@gmail.com
    > Created Time: Sat 17 Sep 2016 03:52:51 PM CDT
 ************************************************************************/

#include <iostream>
#include <cstdlib>
#include <time.h>

double MonteCarloPi(int s) {
    int c = 0;
    int i;
#pragma omp parallel for reduction(+:c)
    for (i = 0; i < s; i++) {
        double x = (double)rand() / RAND_MAX;
        double y = (double)rand() / RAND_MAX;
        if (x * x + y * y < 1)
            c = c + 1;
    }
    return (double)c / s * 4;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));
    std::cout << "PI is " << MonteCarloPi(atoi(argv[1])) << std::endl;
}

