package q2;

// TODO
// Use MyLock to protect the count

public class LockCounter extends Counter {

    MyLock lock;
    public LockCounter(MyLock lock) {
        this.lock = lock;
        count = 0;
    }

    @Override
    public void increment(int id) {
        lock.lock(id);
        ++count;
        lock.unlock(id);
    }

    @Override
    public int getCount() {
        return count;
    }
}
