package q2;


public class Main {
    public static void main (String[] args) {
        Counter counter;
        MyLock lock = null;
        long executeTimeMS = 0;

        if (args.length < 3) {
            System.err.println("Provide 3 arguments");
            System.err.println("\t(1) <algorithm>: fast/bakery/synchronized/"
                    + "reentrant");
            System.err.println("\t(2) <numThread>: the number of test thread");
            System.err.println("\t(3) <numTotalInc>: the total number of "
                    + "increment operations performed");
            System.exit(-1);
        }

        int numThread = Integer.parseInt(args[1]);
        int numTotalInc = Integer.parseInt(args[2]);


        counter = new LockCounter(new FastMutexLock(numThread));
        if (args[0].equals("fast")) {
            lock = new FastMutexLock(numThread);
            counter = new LockCounter(lock);
        } else if (args[0].equals("bakery")) {
            lock = new BakeryLock(numThread);
            counter = new LockCounter(lock);
        } else {
            System.err.println("ERROR: no such algorithm implemented");
            System.exit(-1);
        }



        // TODO
        // Please create numThread threads to increment the counter
        // Each thread executes numTotalInc/numThread increments
        // Please calculate the total execute time in millisecond and store the
        // result in executeTimeMS
        System.out.println(numThread + " " + numTotalInc);
        PIncrement[] t = new PIncrement[numThread];

        executeTimeMS = System.currentTimeMillis();
        for(int i = 0; i < numThread; ++i){
            t[i] = new PIncrement(i, numTotalInc/numThread, counter); //set thread id = i
            t[i].start();
        }
        for(int i = 0; i < numThread; ++i){
            try{
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(counter.getCount());
        executeTimeMS = System.currentTimeMillis() - executeTimeMS;
        System.out.println(executeTimeMS);
    }
}

class PIncrement extends Thread{
    int myId;
    int times;
    Counter counter;
    public PIncrement(int id, int times, Counter counter){
        myId = id;
        this.times = times;
        this.counter = counter;
    }
    public void run() {
        for(int i = 0; i < times; ++i) {
            counter.increment(myId);
        }
    }
}


