package q2;

import java.util.concurrent.atomic.AtomicBoolean;

// TODO
// Implement Fast Mutex Algorithm
public class FastMutexLock implements MyLock {
    volatile int X, Y;
    int N;
    AtomicBoolean[] flag;
    public FastMutexLock(int numThread) {
        N = numThread;
        X = -1;
        Y = -1;
        flag = new AtomicBoolean[N];
        for (int i = 0; i < N; ++i)
            flag[i] = new AtomicBoolean();
    }

    @Override
    public void lock(int myId) {
        while(true){
            flag[myId].set(true);
            X = myId;
            if(Y != -1) {  // go left
                flag[myId].set(false);
                while(Y!= -1) ;
                continue;
            } else {
                Y = myId;
                if (X == myId)
                    return; //go down
                else {
                    flag[myId].set(false);
                    for(int j = 0; j < N; ++j){
                        while(flag[j].get()) ;
                    }
                    if(Y == myId)
                        return;
                    else {
                        while(Y != -1);
                        continue;
                    }
                }
            }
        }
    }

    @Override
    public void unlock(int myId) {
        Y = -1;
        flag[myId].set(false);
    }
}
