package q2;

public abstract class Counter {
    public Counter() {
        count = 0;
    }
    protected volatile int count;
    public abstract void increment(int id);
    public abstract int getCount();
}
