package q1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Shengwei_Wang on 9/25/16.
 */
public class Splitter {

    AtomicInteger[] door;
    AtomicInteger[] last;
    boolean[][] flag;
    boolean[] chosen;
//    int[] old;
    int n;
    public Splitter(int n){
        this.n = n;
        door = new AtomicInteger[n];
        last = new AtomicInteger[n];
        flag = new boolean[n][n];
        chosen = new boolean[n * (n + 1)/2 + 1];
//        old = new int[n];
//        for(int i = 0; i < n; ++i) {
//            old[i] = i + 1;
//            chosen[i + 1] = true;
//        }
        for(int i = 0; i < n; ++i){
            door[i] = new AtomicInteger(-1);
            last[i] = new AtomicInteger();
        }
    }

    public int rename(int pid){
//        chosen[old[pid]] = false; //return the original value;
        for(int i = 0; i < n; ++i){
            flag[i][pid] = true;
            last[i].set(pid);
            if(door[i].get() != -1){
                flag[i][pid] = false;
                continue;   //turn left, to next set
            } else {
                door[i].set(pid);
                if(last[i].get() == pid) {  //turn down to choose from a set size of n - i
                    return getAndRelease(pid, i);
                }
                else { // turn right
                    flag[i][pid] = false;
                    for(int j = 0; j < n; ++j)
                        while(flag[i][j]) ; //busy waiting
                    if(door[i].get() == pid){
                        return getAndRelease(pid, i);
                    } else {
                            //start over again
                        continue;
                    }
                }
            }
        }
        return -1;

    }

    public int getAndRelease(int pid, int i){
        for(int j = n * (n + 1)/2 - ((n-i) * (n-i+1)/2) + 1; j < n * (n + 1)/2 - ((n-i) * (n-i+1)/2) + n - i + 1; ++j){ //in the i th group
            if(!chosen[j]){
                chosen[j] = true;
//                old[pid] = j;
                door[i].set(-1);
                flag[i][pid] = false;
                return j;
            }
        }
        return -1;
    }

}
