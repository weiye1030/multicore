package q1;


/**
 * Created by Shengwei_Wang on 9/25/16.
 */
class Renaming extends Thread{

    public static void renaming(int numThreads){
        Renaming[] t = new Renaming[numThreads];
        Splitter sp = new Splitter(numThreads);
        for(int i = 0; i < numThreads; ++i){
            t[i] = new Renaming(i, sp); //set thread id = i
            t[i].start();
        }
        for(int i = 0; i < numThreads; ++i){
            try{
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }

    }

    int myId;
    Splitter sp;
    public Renaming(int id, Splitter sp){
        myId = id;
        this.sp = sp;
    }
    public void run() {
        System.out.println("Thread id: " + myId + " new name " + sp.rename(myId));
    }

}

public class testSplitter {
    public static void main(String[] args) throws Exception {
        Renaming.renaming(180);
    }
}