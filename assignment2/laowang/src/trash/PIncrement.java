//package trash;
//
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.concurrent.atomic.AtomicInteger;
//
///**
// * Created by Shengwei_Wang on 9/20/16.
// */
//class PIncrement_Bakery extends Thread{
//    public static int c = 0;
//    public static int parallelIncrement(int c, int numThreads){
//        q2.PIncrement_Bakery[] t = new q2.PIncrement_Bakery[numThreads];
//        q2.Bakery lock = new q2.Bakery(numThreads);
//        q2.PIncrement_Bakery.c = c;
//        for(int i = 0; i < numThreads; ++i){
//            t[i] = new q2.PIncrement_Bakery(i, 1200000/numThreads, lock); //set thread id = i
//            t[i].start();
//        }
//        for(int i = 0; i < numThreads; ++i){
//            try{
//                t[i].join();
//            } catch (Exception e) {
//                System.err.println(e);
//            }
//        }
//        System.out.println(q2.PIncrement_Bakery.c);
//
//        return q2.PIncrement_Bakery.c;
//    }
//
//    int myId;
//    int times;
//    q2.Bakery lock;
//    public PIncrement_Bakery(int id, int times, q2.Bakery lock){
//        myId = id;
//        this.times = times;
//        this.lock = lock;
//    }
//    public void run() {
//        for(int i = 0; i < times; ++i){
//            lock.requestCS(myId);
//            ++c;
//            lock.releaseCS(myId);
//        }
//    }
//
//}
//
//class PIncrement_FastMutex extends Thread{
//    public static int c = 0;
//    public static int parallelIncrement(int c, int numThreads){
//        PIncrement_FastMutex[] t = new PIncrement_FastMutex[numThreads];
//        FastMutex lock = new FastMutex(numThreads);
//        PIncrement_FastMutex.c = c;
//        for(int i = 0; i < numThreads; ++i){
//            t[i] = new q2.PIncrement_FastMutex(i, 1200000/numThreads, lock); //set thread id = i
//            t[i].start();
//        }
//        for(int i = 0; i < numThreads; ++i){
//            try{
//                t[i].join();
//            } catch (Exception e) {
//                System.err.println(e);
//            }
//        }
//        System.out.println(q2.PIncrement_FastMutex.c);
//
//        return q2.PIncrement_FastMutex.c;
//    }
//
//    int myId;
//    int times;
//    q2.FastMutex lock;
//    public PIncrement_FastMutex(int id, int times, q2.FastMutex lock){
//        myId = id;
//        this.times = times;
//        this.lock = lock;
//    }
//    public void run() {
//        for(int i = 0; i < times; ++i){
//            lock.requestCS(myId);
//            ++c;
//            lock.releaseCS(myId);
//        }
//    }
//
//}
//
//interface Lock {
//    void requestCS(int pid);
//    void releaseCS(int pid);
//}
//
//
//class Bakery implements q2.Lock {
//    int N;
//    AtomicBoolean[] choosing;
//    AtomicInteger[] number;
//    Bakery(int numProc){
//        N = numProc;
//        choosing = new AtomicBoolean[N];
//        number = new AtomicInteger[N];
//        for(int i = 0; i < N; ++i){
//            choosing[i] = new AtomicBoolean();
//            number[i] = new AtomicInteger();
//        }
//    }
//    public void requestCS(int i){
//        choosing[i].set(true);
//        for (int j = 0; j < N; ++j){
//            int ni = number[i].get();
//            int nj = number[j].get();
//            if (nj > ni){
//                number[i].set(nj);
//            }
//        }
//        number[i].getAndIncrement();
//        choosing[i].set(false);
//
//        for (int j = 0; j < N; ++j) {
//            while (choosing[j].get()) ;
//            while (true) {
//                int ni = number[i].get();
//                int nj = number[j].get();
//
//                if(! (nj != 0 && (nj < ni || (nj == ni && j < i))))
//                    break;
//            }
//        }
//    }
//
//    public void releaseCS(int i){
//        number[i].set(0);
//    }
//}
//
//
//class FastMutex {
//    volatile int X, Y;
//    int N;
//    AtomicBoolean[] flag;
//    FastMutex(int numProc){
//        N = numProc;
//        X = -1;
//        Y = -1;
//        flag = new AtomicBoolean[N];
//        for (int i = 0; i < N; ++i)
//            flag[i] = new AtomicBoolean();
//    }
//
//    public void requestCS(int i){
//        while(true){
//            flag[i].set(true);
//            X = i;
//            if(Y != -1) {  // go left
//                flag[i].set(false);
//                while(Y!= -1) ;
//                continue;
//            } else {
//                Y = i;
//                if (X ==i)
//                    return; //go down
//                else {
//                    flag[i].set(false);
//                    for(int j = 0; j < N; ++j){
//                        while(flag[j].get()) ;
//                    }
//                    if(Y == i)
//                        return;
//                    else {
//                        while(Y != -1);
//                        continue;
//                    }
//                }
//            }
//        }
//    }
//
//    public void releaseCS(int i){
//        Y = -1;
//        flag[i].set(false);
//    }
//}
//
//public class PIncrement {
//    public static void main(String[] args) throws Exception {
//        long start;
//        long end;
//        int n = 1;
//        int c = 0;
//        while(n <= 8){
//
//            System.out.println(n + " Threads: ");
//            System.out.println("Test for FastMutex: ");
//            start = System.currentTimeMillis();
//            q2.PIncrement_FastMutex.parallelIncrement(c, n);
//            end = System.currentTimeMillis();
//            System.out.println("part a finished Time: " + (end - start));
//
//
//            System.out.println(n + " Threads: ");
//            System.out.println("Test for Bakery: ");
//            start = System.currentTimeMillis();
//            q2.PIncrement_Bakery.parallelIncrement(c, n);
//            end = System.currentTimeMillis();
//            System.out.println("part a finished Time: " + (end - start));
//
//            n *=2;
//
//        }
//    }
//}