package q3;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Shengwei_Wang on 10/8/16.
 */
public class Garden {

// you are free to add members




    Lock mutex = new ReentrantLock();
    Condition digWait = mutex.newCondition();
    Condition seedWait = mutex.newCondition();
    Condition fillWait = mutex.newCondition();
    int max;
    int digging_n = 0;
    int seeding_n = 0;

    public Garden (int MAX) {
        this.max = MAX;
    }
    void startDigging() throws InterruptedException{
        mutex.lock();
        try {
            while(digging_n == max)
                digWait.await();
        } finally {
            mutex.lock();
        }
    }

    void doneDigging(){
        mutex.lock();
        try{
            ++digging_n;
            seedWait.signal();
        } finally {
            mutex.unlock();
        }
    }

    void startSeeding() throws InterruptedException{
        mutex.lock();
        try{
            while (digging_n == seeding_n)
                seedWait.await();
        } finally {
            mutex.unlock();
        }
    }

    void doneSeeding(){
        mutex.lock();
        try{
            ++seeding_n;
            fillWait.signal();
        } finally {
            mutex.unlock();
        }
    }

    void startFilling() throws InterruptedException{
        mutex.lock();
        try {
            while(seeding_n == 0)
                fillWait.await();
        } finally {
            mutex.lock();
        }
    }

    void doneFilling(){
        mutex.lock();
        try {
            --seeding_n;
            --digging_n;
            digWait.signal();
        } finally {
            mutex.lock();
        }
    }

protected static class Newton implements Runnable {
        Garden garden;
        int digged = 0;
        public Newton (Garden garden){
            this.garden = garden;
        }

        @Override
        public void run(){
            while (true) {
                try {
                    garden.startDigging();
                    dig();
                } catch (Exception e){
                } finally {
                    garden.doneDigging();
                }

            }
        }

        private void dig(){
            Util.mySleep (200);
            System.out.println("Digging : " + digged++);
        }
    }

    static class Benjamin implements Runnable{
        Garden garden;
        int seeded = 0;
        public Benjamin (Garden garden){
            this.garden = garden;
        }

        @Override
        public void run(){
            while (true) {
                try {
                    garden.startSeeding();
                    plantSeed();
                } catch (Exception e){
                } finally {
                    garden.doneSeeding();
                }

            }
        }

        private void plantSeed(){
            Util.mySleep (20);
            System.out.println("plantSeed : " + seeded++);
        }
    }

    static class Mary implements Runnable{
        Garden garden;
        int filled = 0;
        public Mary (Garden garden){
            this.garden = garden;
        }

        @Override
        public void run(){
            while (true) {
                try {
                    garden.startFilling();
                    Fill();
                } catch (Exception e){
                } finally {
                    garden.doneFilling();
                }

            }
        }

        private void Fill(){
            Util.mySleep(200);
            System.out.println("Filling : " + filled++);
        }
    }

    public static void main(String args[]) {
        int max = 3;
        Garden garden = new Garden(max);
        Garden.Newton myNewton = new Garden.Newton(garden);
        Garden.Benjamin myBenjamin = new Garden.Benjamin(garden);
        Garden.Mary myMary = new Garden.Mary(garden);

        Thread t1 = new Thread(myNewton);
        Thread t2 = new Thread(myBenjamin);
        Thread t3 = new Thread(myMary);

        t1.start();
        t2.start();
        t3.start();
    }
}
