package q4;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class LockFreeListSet implements ListSet {
// you are free to add members

  AtomicMarkableReference<Node> head;
  AtomicMarkableReference<Node> tail;
  public LockFreeListSet() {
    // implement your constructor here
    head = new AtomicMarkableReference<Node>(new Node(0), false);
    tail = new AtomicMarkableReference<Node>(new Node(0), false);
    head.getReference().next = new AtomicMarkableReference<>(tail.getReference(), false);
  }
	  
  public boolean add(int value) {
	// implement your add method here
    AtomicMarkableReference<Node> node = new AtomicMarkableReference<Node>(new Node(value), false);
    while(true) {
      AtomicMarkableReference<Node> prec = new AtomicMarkableReference<Node>(head.getReference(), head.isMarked());
      AtomicMarkableReference<Node> succ = new AtomicMarkableReference<>(head.getReference().next.getReference(), head.getReference().next.isMarked());
      while (succ.getReference() != tail.getReference()) {
        if(succ.getReference().value == value){
          return false;
        } else if (succ.getReference().value > value){
          break;
        } else {
          prec = succ;
          succ = succ.getReference().next;
        }
      }
      node.getReference().next = new AtomicMarkableReference<>(succ.getReference(), false);
      if(prec.getReference().next.compareAndSet(succ.getReference(), node.getReference(), false, false))
        return true;
    }
  }
	  
  public boolean remove(int value) {
    // implement your remove method here	
	return false;
  }
	  
  public boolean contains(int value) {
	// implement your contains method here
    Node cur = head.getReference().next.getReference();
    while(cur != tail.getReference() && cur.value < value){
      cur = cur.next.getReference();
    }
    if(cur != tail.getReference() && cur.value == value /*&& !cur.isDeleted*/)
      return true;
    else
      return false;

  }
	  
  protected class Node {
	  public Integer value;
	  public AtomicMarkableReference<Node> next;
			    
	  public Node(Integer x) {
		  value = x;
		  next = null;
	  }
  }
}
