package q4;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CoarseGrainedListSet implements ListSet {
// you are free to add members

  Lock mutex;
  Node head;
  public CoarseGrainedListSet() {
	// implement your constructor here
    head = new Node(0);
    mutex = new ReentrantLock();
  }
  
  public boolean add(int value) {
	// implement your add method here
    mutex.lock();

    boolean flag;
    Node pre = head;
    Node cur = head.next;
    while (cur != null && cur.value < value) {
      pre = cur;
      cur = cur.next;
    }
    if(cur != null && cur.value == value){
      flag = false;
    } else {
      flag = true;
      Node newNode = new Node(value);
      newNode.next = pre.next;
      pre.next = newNode;
    }

    mutex.unlock();
    return flag;
  }
  
  public boolean remove(int value) {
	// implement your remove method here
    mutex.lock();

    boolean flag;
    Node pre = head;
    Node cur = head.next;
    while (cur != null && cur.value < value) {
      pre = cur;
      cur = cur.next;
    }
    if(cur != null && cur.value == value){
      pre.next = pre.next.next;
      flag = true;
    } else {
      flag = false;
    }

    mutex.unlock();
    return flag;
  }
  
  public boolean contains(int value) {
	// implement your contains method here
    mutex.lock();
    boolean flag;
    Node cur = head.next;
    while(cur != null && cur.value < value){
      cur = cur.next;
    }
    if(cur != null && cur.value == value) {
      flag = true;
    } else {
      flag = false;
    }
    mutex.unlock();
    return flag;
  }
  
  protected class Node {
	  public Integer value;
	  public Node next;
		    
	  public Node(Integer x) {
		  value = x;
		  next = null;
	  }
  }
}
