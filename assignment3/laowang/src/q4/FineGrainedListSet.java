package q4;
import java.util.concurrent.locks.ReentrantLock;

public class FineGrainedListSet implements ListSet {
// you are free to add members
  Node head;
  Node tail;
  public FineGrainedListSet() {
    // implement your constructor here
    head = new Node(0);
    tail = new Node(0);
    head.next = tail;
  }
	  
  public boolean add(int value) {
    // implement your add method here
    while(true) {
      Node prev = head;
      Node succ = head.next;
      while (succ != tail && succ.value < value) {
        prev = succ;
        succ = succ.next;
      }
      prev.lock.lock();
      try {
        succ.lock.lock();
        try {
          if (!prev.isDeleted && !succ.isDeleted && prev.next == succ) {
            if (succ == tail || succ.value > value) {
              Node node = new Node(value);
              node.next = succ;
              prev.next = node;
              return true;
            } else if (succ.value == value)
              return false;
          }
        } finally {
          succ.lock.unlock();
        }
      } finally {
        prev.lock.unlock();
      }
    }
  }
	  
  public boolean remove(int value) {
    // implement your remove method here
    while (true){
      Node prev = head;
      Node curr = prev.next;
      while (curr != tail && curr.value < value) {
        prev = curr;
        curr = curr.next;
      }
      if (curr == tail || curr.value > value || curr.isDeleted)
        return false;
      prev.lock.lock();
      try {
        curr.lock.lock();
        try {
          if (!prev.isDeleted && !curr.isDeleted && prev.next == curr) {
            curr.isDeleted = true;
            prev.next = curr.next;
            return true;
          }
        } finally {
          curr.lock.unlock();
        }
      } finally {
        prev.lock.unlock();
      }
    }
  }
	  
  public boolean contains(int value) {
    // implement your contains method here

    Node curr = head.next;
    while(curr != tail && curr.value < value){
      curr = curr.next;
    }
    if(curr != tail && curr.value == value && !curr.isDeleted)
      return true;
    else
      return false;
  }

  protected class Node {
    public Integer value;
	public Node next;
    public boolean isDeleted = false;
    ReentrantLock lock = new ReentrantLock();
	public Node(Integer x) {
		value = x;
		next = null;
	}
  }
}
