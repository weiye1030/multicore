package q4;


/**
 * Created by Shengwei_Wang on 10/16/16.
 */
public class listTester extends Thread{
    static volatile int enqn = 0;
    static volatile int deqn = 0;
    ListSet list;
    int id;
    public static void main(String[] args){
        int n = 8;
        ListSet list = new FineGrainedListSet();
        listTester[] threads = new listTester[n];
        for(int i = 0; i < n; ++i)
            threads[i] = new listTester(list, i);
        for(int i = 0; i < n; ++i)
            threads[i].start();
        for(int i = 0; i < n; ++i)
            try {
                threads[i].join();
            } catch (Exception e) {

            }

        System.out.println("all finished");
        for(int i = 0; i <= 10 * (8 + 10); ++i)
            if (list.contains(i)) {
                System.out.println(i);
                deqn += i;
            }

        System.out.println(enqn);
        System.out.println(deqn);

    }
    public listTester(ListSet list, int id){
        this.list = list;
        this.id = id;
    }
    public void run() {
        for (int k = 2; k < 10; k++) {
            if (k % 2 == 0) {
                System.out.println("Thread " + id + "add: ");
                if (list.add(k * (id + 10))) {
                    enqn += k * (id + 10);
                    System.out.println(k * (id + 10));
                } else {
                    System.out.println("exist" + k * (id + 10));
                }
            } else {
//                System.out.println("Thread " + id + "deq: ");
//                deqn += queue.deq();
////                System.out.println(queue.deq());
            }
        }
        try{
            Thread.sleep(1000);
        } catch (Exception e) {

        }
        for (int k = 2; k < 10; k++) {
            if (k % 2 == 0) {
                System.out.println("Thread " + id + "Delete: ");
                if (list.remove(k * (id + 10))) {
                    deqn += k * (id + 10);
                    System.out.println(k * (id + 10));
                } else {
                    System.out.println("not found" + k * (id + 10));
                }
            } else {
//                System.out.println("Thread " + id + "deq: ");
//                deqn += queue.deq();
////                System.out.println(queue.deq());
            }
        }
    }

}
