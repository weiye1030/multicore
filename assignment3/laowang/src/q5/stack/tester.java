package q5.stack;


/**
 * Created by Shengwei_Wang on 10/16/16.
 */
public class tester extends Thread{
    static volatile int enqn = 0;
    static volatile int deqn = 0;
    MyStack stack;
    int id;
    public static void main(String[] args){
        int n = 8;
        MyStack stack = new LockFreeStack();
        tester[] threads = new tester[n];
        for(int i = 0; i < n; ++i)
            threads[i] = new tester(stack, i);
        for(int i = 0; i < n; ++i)
            threads[i].start();
        for(int i = 0; i < n; ++i)
            try {
                threads[i].join();
            } catch (Exception e) {

            }
//        System.out.println("push finished!");
        while(true){
            try{
                System.out.println(stack.pop());
            } catch (Exception e){
                break;
            }
        }
        System.out.println(enqn);
        System.out.println(deqn);

    }
    public tester(MyStack stack, int id){
        this.stack = stack;
        this.id = id;
    }
    public void run() {
        for (int k = 2; k < 10; k++) {
            if (k % 2 == 0) {
                System.out.println("Thread " + id + "push: ");
                stack.push(k);
                enqn += k;
//                System.out.println(k);
            } else {
//                System.out.println("Thread " + id + "pop: ");
//                try {
//                    deqn += stack.pop();
//                } catch (Exception e){
//                    System.out.println("Empty");
//                }
//                System.out.println(Stack.deq());
            }
        }
    }

}
