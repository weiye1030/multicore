package q5.stack;

import java.util.NoSuchElementException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockStack implements MyStack {
// you are free to add members
 Lock lock;
    Node top;
  public LockStack() {
	  // implement your constructor here
      lock = new ReentrantLock();
      top = null;
  }
  
  public boolean push(Integer value) {
	  // implement your push method here
      lock.lock();
      Node node = new Node(value);
      node.next = top;
      top = node;
      lock.unlock();
	  return true;
  }
  
  public Integer pop() throws EmptyStack {
	  // implement your pop method here
      int ans;
      lock.lock();
      try {
          if (top == null)
              throw new EmptyStack();
          else {
              ans = top.value;
              top = top.next;
          }
      } finally {
          lock.unlock();
      }
      return ans;
  }
  
  protected class Node {
	  public Integer value;
	  public Node next;
		    
	  public Node(Integer x) {
		  value = x;
		  next = null;
	  }
  }
}
