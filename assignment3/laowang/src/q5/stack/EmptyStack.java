package q5.stack;

// This is the exception throwed when try to pop an
// element from an empty stack.
public class EmptyStack extends Exception {
  public EmptyStack() {
    super ();
  }
  
}
