package q5.queue;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockQueue implements MyQueue {
// you are free to add members

  AtomicInteger count = new AtomicInteger(0);
  Node head;
  Node tail;
  Lock enqLock, deqLock;
  Condition isEmpty;
  public LockQueue() {
	// implement your constructor here
    head = new Node(0);
    tail = head;
    enqLock = new ReentrantLock();
    deqLock = new ReentrantLock();
    isEmpty = deqLock.newCondition();
  }
  
  public boolean enq(Integer value) {
	// implement your enq method here
    enqLock.lock();
    try{
      Node cur = new Node(value);
      tail.next = cur;
      tail = cur;
      if (count.getAndIncrement() == 0){
        deqLock.lock();
        isEmpty.signalAll();
        deqLock.unlock();
      }

    } finally {
      enqLock.unlock();
    }
    return true;
  }
  
  public Integer deq() {
	// implement your deq method here
    int res = -1;
    deqLock.lock();
    try{
      while(head.next == null)
          isEmpty.await();
      res = head.next.value;
      head = head.next;
      count.getAndDecrement();
    } catch (Exception e){

    }finally {
      deqLock.unlock();
    }
    return res;
  }
  
  protected class Node {
	  public Integer value;
	  public Node next;
		    
	  public Node(Integer x) {
		  value = x;
		  next = null;
	  }
  }
}
