package q5.queue;


import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

public class LockFreeQueue implements MyQueue {
// you are free to add members
  AtomicStampedReference<Node> head, tail;
  AtomicInteger count;
  public LockFreeQueue() {
	// implement your constructor here
    Node node = new Node(0);
//    count = new AtomicInteger(0);
    head = new AtomicStampedReference<Node>(node, 0);
    tail = new AtomicStampedReference<Node>(node, 0);


  }

  public boolean enq(Integer value) {
	// implement your enq method here
    AtomicStampedReference<Node> node = new AtomicStampedReference<Node>(new Node(value), 0);
    AtomicStampedReference<Node> tail;
    AtomicStampedReference<Node> next;
    while(true){
      tail = new AtomicStampedReference<>(this.tail.getReference(), this.tail.getStamp());
      next = new AtomicStampedReference<>(tail.getReference().next.getReference(), tail.getReference().next.getStamp());
      if (tail.getReference() == this.tail.getReference() && tail.getStamp() == this.tail.getStamp()) {
        if (next.getReference() == null) {
          if (tail.getReference().next.compareAndSet(next.getReference(), node.getReference(), next.getStamp() , next.getStamp() + 1))
            break;
        } else {
          this.tail.compareAndSet(tail.getReference(), next.getReference(), tail.getStamp(), tail.getStamp() + 1);
        }
      }
    }
//    System.out.println(this.tail.getStamp() + " " + tail.getStamp());
    this.tail.compareAndSet(tail.getReference(), node.getReference(), tail.getStamp(), tail.getStamp() + 1);
//    System.out.println(this.head.getReference().value + " " + this.head.getReference().next.getReference().value + " " + this.tail.getReference().value);
    return true;
  }
  
  public Integer deq() {
	// implement your deq method here
    int ans;
    while (true) {
      try {
//        Thread.sleep(1000);
      } catch (Exception e){

      }
      AtomicStampedReference<Node> head = new AtomicStampedReference<>(this.head.getReference(), this.head.getStamp());
      AtomicStampedReference<Node> tail = new AtomicStampedReference<>(this.tail.getReference(), this.tail.getStamp());
      AtomicStampedReference<Node> next = head.getReference().next;
//      System.out.println(head.getReference().value + " " + head.getReference().next.getReference().value + " " + tail.getReference().value);
      if (head.getReference() == this.head.getReference()) {
        if (head.getReference() == tail.getReference()) {
          if (next.getReference() == null)
            Thread.yield(); //busy waiting
          else
            this.tail.compareAndSet(tail.getReference(), next.getReference(), this.tail.getStamp(), tail.getStamp() + 1);
        } else {
          ans = next.getReference().value;
          if (this.head.compareAndSet(head.getReference(), next.getReference(), this.head.getStamp(), head.getStamp() + 1))
            break;
        }
      }
    }
    return ans;
  }
  
  protected class Node {
	  public Integer value;
	  public AtomicStampedReference<Node> next;
		    
	  public Node(Integer x) {
		  value = x;
		  next = new AtomicStampedReference<>(null, 0);
	  }
  }
}
