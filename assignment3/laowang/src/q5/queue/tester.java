package q5.queue;

/**
 * Created by Shengwei_Wang on 10/16/16.
 */
public class tester extends Thread{
    static volatile int enqn = 0;
    static volatile int deqn = 0;
    MyQueue queue;
    int id;
    public static void main(String[] args){
        int n = 8;
        MyQueue queue = new LockFreeQueue();
        tester[] threads = new tester[n];
        for(int i = 0; i < n; ++i)
            threads[i] = new tester(queue, i);
        for(int i = 0; i < n; ++i)
            threads[i].start();
        for(int i = 0; i < n; ++i)
            try {
                threads[i].join();
            } catch (Exception e) {

            }
        System.out.println(enqn);
        System.out.println(deqn);

    }
    public tester (MyQueue queue, int id){
        this.queue = queue;
        this.id = id;
    }
    public void run() {
        for (int k = 2; k < 10; k++) {
            if (k % 2 == 0) {
                System.out.println("Thread " + id + "enq: ");
                queue.enq(k);
                enqn += k;
//                System.out.println(k);
            } else {
                System.out.println("Thread " + id + "deq: ");
                deqn += queue.deq();
//                System.out.println(queue.deq());
            }
        }
    }

}
