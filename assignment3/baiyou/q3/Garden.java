package q3;
import java.util.concurrent.locks.*;

public class Garden {
// you are free to add members
        ReentrantLock lock = new ReentrantLock();
        Condition dig = lock.newCondition();
        Condition seed = lock.newCondition();
        Condition fill = lock.newCondition();
        int numHolesToSeed = 0;
        int numHolesToFill = 0;
        boolean availShovel = true;
        final int MAX = 3;

	public Garden(){
		// implement your constructor here
	}
	public void startDigging() throws InterruptedException{
        lock.lock();
        try {
            while (!availShovel || (numHolesToSeed + numHolesToFill) >= MAX) {
                dig.await();
            }
            availShovel = false;
        } finally {
            lock.unlock();
        }
	}
	public void doneDigging(){
        lock.lock();
        try {
            availShovel = true;
            numHolesToSeed++;
            seed.signal(); // finish digging a hole, can seed now
            fill.signal(); // finish using the shovel, can fill now
        } finally {
            lock.unlock();
        }
	} 
	public void startSeeding() throws InterruptedException{
        lock.lock();
        try {
            while (numHolesToSeed <= 0) {
                seed.await();
            }
        } finally {
            lock.unlock();
        }
	}
	public void doneSeeding(){
        lock.lock();
        try {
            numHolesToSeed--;
            numHolesToFill++;
            fill.signal();
        } finally {
            lock.unlock();
        }
    } 
	public void startFilling() throws InterruptedException{
        lock.lock();
        try {
            while (!availShovel || numHolesToFill <= 0) {
                fill.await();
            }
            availShovel = false;
        } finally {
            lock.unlock();
        }
	}
	public void doneFilling(){
        lock.lock();
        try {
            availShovel = true;
            numHolesToFill--;
            dig.signal(); // finish using the shovel, can dig now
        } finally {
            lock.unlock();
        }
	}

// You are free to implements your own Newton, Benjamin and Mary
// classes. They will NOT count to your grade.
	protected static class Newton implements Runnable {
        Garden garden;
        int digged = 0;
		public Newton(Garden garden){
			this.garden = garden;
		}
		@Override
		public void run() {
		    while (true) {
                try {
                    garden.startDigging();
                } catch (InterruptedException e) {}
			    dig();
				garden.doneDigging();
			}
		} 
		
		private void dig(){
            Util.mySleep (200);
            System.out.println("Digging : " + digged++);
		}
	}
	
	protected static class Benjamin implements Runnable {
        Garden garden;
        int seeded = 0;
		public Benjamin(Garden garden){
			this.garden = garden;
		}
		@Override
		public void run() {
		    while (true) {
                try {
                    garden.startSeeding();
                } catch (InterruptedException e) {}
                plantSeed();
				garden.doneSeeding();
			}
		} 
		
		private void plantSeed(){
            Util.mySleep (20);
            System.out.println("plantSeed : " + seeded++);
		}
	}
	
	protected static class Mary implements Runnable {
        Garden garden;
        int filled = 0;
		public Mary(Garden garden){
            this.garden = garden;
		}
		@Override
		public void run() {
		    while (true) {
                try {
                    garden.startFilling();
                } catch (InterruptedException e) {}
                Fill();
			 	garden.doneFilling();
			}
		} 
		
		private void Fill(){
            Util.mySleep(200);
            System.out.println("Filling : " + filled++);
		}
	}

    //TODO: delete
    public static void main(String args[]) {
        Garden garden = new Garden();
        Garden.Newton myNewton = new Garden.Newton(garden);
        Garden.Benjamin myBenjamin = new Garden.Benjamin(garden);
        Garden.Mary myMary = new Garden.Mary(garden);

        Thread t1 = new Thread(myNewton);
        Thread t2 = new Thread(myBenjamin);
        Thread t3 = new Thread(myMary);

        t1.start();
        t2.start();
        t3.start();
    }
}
