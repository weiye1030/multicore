package q4;

public class CoarseGrainedListSet implements ListSet {
// you are free to add members
  Node head;
  Node tail;
	
  public CoarseGrainedListSet() {
	// implement your constructor here	  
    head = new Node(0);
    tail = new Node(0);
    head.next = tail;
  }
  
  public synchronized boolean add(int value) {
	// implement your add method here
    //System.out.println("debug|add " + value);
    Node current = new Node(value);
    Node prev = head;
    while (prev.next != tail && prev.next.value < value) {
        prev = prev.next;
    }
    current.next = prev.next;
    prev.next = current;

    return true;
  }
  
  public synchronized boolean remove(int value) {
	// implement your remove method here	
    //System.out.println("debug|remove " + value);
    if (!contains(value))
        return false;
    
    Node prev = head;
    while (prev.next.value != value && prev.next != tail) 
        prev = prev.next;
    
    Node current = prev.next;
    prev.next = current.next;
    return true;
  }
  
  public synchronized boolean contains(int value) {
	// implement your contains method here	
    //System.out.println("debug|contains " + value);
    Node curr = head.next;
    while (curr != tail && curr.value <= value) {
        if (curr.value == value)
            return true;
        curr = curr.next;
    }
    return false;
  }
  
  protected class Node {
	  public Integer value;
	  public Node next;
		    
	  public Node(Integer x) {
		  value = x;
		  next = null;
	  }
  }
}
