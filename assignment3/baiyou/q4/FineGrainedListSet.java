package q4;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FineGrainedListSet implements ListSet {
// you are free to add members
  public Node head;
  public Node tail;

  public FineGrainedListSet() {
    // implement your constructor here	  
    head = new Node(0);
    tail = new Node(0);
    head.next = tail;
  }
	  
  public boolean add(int value) {
    // implement your add method here	
    //return false;
    while (true) {
        //System.out.println("debug|add in loop " + value);
        Node curr = new Node(value);
        Node prev = head;
        while (prev.next != tail && prev.next.value < value) {
            prev = prev.next;
        }
        Node succ = prev.next;
        prev.lock.lock();
        succ.lock.lock();
        boolean ok = false;
        try {
            if (!prev.isDeleted && !succ.isDeleted && prev.next == succ) {
                curr.next = succ;
                prev.next = curr;
                ok = true;
            } 
        } finally {
            prev.lock.unlock();
            succ.lock.unlock();
        }
        if (ok) break;
    }
    return true;
  }
	  
  public boolean remove(int value) {
      // implement your remove method here	
      //return false;
          //System.out.println("debug|remove in loop " + value);
          Node prev = head;
          prev.lock.lock();
          Node curr = prev.next;
          curr.lock.lock();
          while (curr != tail && curr.value <= value) {
              if (curr.value == value && !curr.isDeleted) {
                  prev.next = curr.next;
                  prev.lock.unlock();
                  curr.lock.unlock();
                  return true;
              }
              prev.lock.unlock();
              prev = curr;
              curr = curr.next;
              curr.lock.lock();
          }
          prev.lock.unlock();
          curr.lock.unlock();
          return false;
  }

  public boolean contains(int value) {
    // implement your contains method here	
    //return false;
    //System.out.println("debug|contains " + value);
    Node curr = head.next;
    while (curr != tail && curr.value <= value) {
        if (curr.value == value && !curr.isDeleted)
            return true;
        curr = curr.next;
    }
    return false;
  }
	  
  //protected class Node {
  public class Node {
    public Integer value;
	public Node next;
    public boolean isDeleted = false;
    public ReentrantLock lock = new ReentrantLock();
			    
	public Node(Integer x) {
		value = x;
		next = null;
	}
  }
}
