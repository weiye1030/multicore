#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 1000000
#define BLOCKSIZE 1024
#define MAXNUM 2000
__global__ void count (int * a, int * b, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		int j = a[i] / 100;
		atomicAdd(b + j, 1);
	}
}

__global__ void count_b (int * a, int * b, int N) {
	__shared__ int tempb[10];
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		int j = a[i] / 100;
		atomicAdd(tempb + j, 1);
	
	__syncthreads();
	if (threadIdx.x < 10)
		atomicAdd(b + threadIdx.x, tempb[threadIdx.x]);
	}
}

__global__ void count_c (int * c, int d) {
	int i = threadIdx.x;
	int val = 0;
	if (i >= d) {
		val = c[i - d];
		__syncthreads();
		c[i] += val;
	}
}

int reader (int * a, int argc, char ** argv) {
	FILE * fp;
	int N = 0;
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error open file\n");
		exit(1);
	}
	while (fscanf(fp, "%d,", &a[N]) != EOF) {
		++N;
	}
	fclose(fp);
	return N;
}

void parta (int * a, int N) {
	int b[10] = {0};
	//cudaMalloc:
	int * d_a;
	int * d_b;
	int size_int = sizeof (int);

	cudaMalloc ((void **) & d_a, N * size_int);
	cudaMalloc ((void **) & d_b, 10 * size_int);

	cudaMemcpy (d_a, a, N * size_int, cudaMemcpyHostToDevice);
	cudaMemcpy (d_b, b, 10 * size_int, cudaMemcpyHostToDevice);
	count<<< (N + BLOCKSIZE - 1) / BLOCKSIZE, BLOCKSIZE>>> (d_a, d_b, N);
	cudaDeviceSynchronize();
	cudaMemcpy (b, d_b, 10 * size_int, cudaMemcpyDeviceToHost);

	cudaFree(d_a);
	cudaFree(d_b);
	FILE * fp;
	fp = fopen("q2a.txt", "w");
    for (int i = 0; i < 9; i++) {
		fprintf(fp, "%d, ", b[i]);
    }
    fprintf(fp, "%d\n", b[9]);
    fclose(fp);
}

void partb (int * a, int * b, int N) {
	//cudaMalloc:
	int * d_a;
	int * d_b;
	int size_int = sizeof (int);

	cudaMalloc ((void **) & d_a, N * size_int);
	cudaMalloc ((void **) & d_b, 10 * size_int);

	cudaMemcpy (d_a, a, N * size_int, cudaMemcpyHostToDevice);
	cudaMemcpy (d_b, b, 10 * size_int, cudaMemcpyHostToDevice);
	count_b<<< (N + BLOCKSIZE - 1) / BLOCKSIZE, BLOCKSIZE>>> (d_a, d_b, N);
	cudaDeviceSynchronize();
	cudaMemcpy (b, d_b, 10 * size_int, cudaMemcpyDeviceToHost);

	cudaFree(d_a);
	cudaFree(d_b);
	FILE * fp;
	fp = fopen("q2b.txt", "w");
    for (int i = 0; i < 9; i++) {
		fprintf(fp, "%d, ", b[i]);
    }
    fprintf(fp, "%d\n", b[9]);
    fclose(fp);
}
void partc (int * b, int * c) {
	int * d_c;
	int size_int = sizeof (int);

	cudaMalloc ((void **) & d_c, 10 * size_int);

	cudaMemcpy (d_c, b, 10 * size_int, cudaMemcpyHostToDevice);
	for (int d = 1; d < 10; d = d * 2) {
		count_c<<<1, 10>>> (d_c, d);
		cudaDeviceSynchronize();
	}
	
	cudaMemcpy (c, d_c, 10 * size_int, cudaMemcpyDeviceToHost);

	cudaFree(d_c);
	FILE * fp;
	fp = fopen("q2c.txt", "w");
    for (int i = 0; i < 9; i++) {
		fprintf(fp, "%d, ", c[i]);
    }
    fprintf(fp, "%d\n", c[9]);
    fclose(fp);
}
int main (int argc, char ** argv) {

	// printf("Hello World\n");
	//read:a
	int a[MAXSIZE];
	int N = reader(a, argc, argv);
	int b[10] = {0};
	int c[10] = {0};
	parta(a, N);
	partb(a, b, N);
	partc(b, c);
	return 0;
}

