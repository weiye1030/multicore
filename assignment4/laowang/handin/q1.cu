#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 1000000
#define BLOCKSIZE 1024
#define MAXNUM 1000

__global__ void reduce_kernel(int * d_out, int * d_in, int n) {
    extern __shared__ int sdata[];

    int myId = threadIdx.x + blockIdx.x * blockDim.x;
    int tid = threadIdx.x;

    sdata[tid] = (myId < n) ? d_in[myId] : MAXNUM;
    __syncthreads();

    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
	if (tid < s) {
            if (sdata[tid + s] < sdata[tid])
	        sdata[tid] = sdata[tid + s];
	    }
	__syncthreads();
    }

    if (tid == 0) {
	d_out[blockIdx.x] = sdata[0];
    }
}

int nearestPowerOf2 (int n) {
    if (!n) return n;

    int x = 1;
    while(x < n) x <<= 1;
    return x;
}

void reduce(int * d_out, int * d_intermediate, int * d_in, int size) {
    int threads = nearestPowerOf2(sqrt(size));
    int blocks = (size + threads - 1) / threads;
    reduce_kernel<<<blocks, threads, threads * sizeof(int)>>>(d_intermediate, d_in, size);

    threads = nearestPowerOf2(blocks);
    reduce_kernel<<<1, threads, threads * sizeof(int)>>>(d_out, d_intermediate, blocks);
}

__global__ void getLastDigit (int * a, int * b) {
	b[blockIdx.x] = a[blockIdx.x] % 10;
}

int main (int argc, char ** argv) {

	//read:
	int a[MAXSIZE];

	int N = 0;
	FILE * fp;
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error open file\n");
		exit(1);
	}
	while (fscanf(fp, "%d,", &a[N]) != EOF) {
		++N;
	}
	fclose(fp);

	int min;
	int b[N];
	int size_int = sizeof (int);
	//cudaMalloc:
	int * d_a;
	int * d_b;
	int * d_intermediate;
	int * d_out;

	cudaMalloc ((void **) & d_a, N * size_int);
	cudaMalloc ((void **) & d_b, N * size_int);
	cudaMalloc ((void **) & d_intermediate, N * size_int);
	cudaMalloc ((void **) & d_out, N * size_int);


	cudaMemcpy (d_a, a, N * size_int, cudaMemcpyHostToDevice);
	reduce(d_out, d_intermediate, d_a, N);
	cudaMemcpy (&min, d_out, size_int, cudaMemcpyDeviceToHost);


	getLastDigit<<<N, 1>>> (d_a, d_b);
	cudaMemcpy (b, d_b, N * size_int, cudaMemcpyDeviceToHost);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_intermediate);
	cudaFree(d_out);


	fp = fopen("q1a.txt", "w");
	fprintf(fp, "%d\n", min);
	fclose(fp);

	fp = fopen("q1b.txt", "w");
    for (int i = 0; i < N - 1; i++) {
		fprintf(fp, "%d, ", b[i]);
    }
    fprintf(fp, "%d\n", b[N - 1]);
    fclose(fp);
	return 0;
}
