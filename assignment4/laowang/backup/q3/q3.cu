#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 100000
#define BLOCKSIZE 64
#define MAXNUM 2000
__global__ void markOdd (int * a, int * b, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		if ((a[i] & 1) == 1)
			b[i] = 1; 
		else
			b[i] = 0;
	}
}

__global__ void scan (int * b, int * tempb, int d, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if ((i < N) && (i >= d)) {
		b[i] += tempb[i - d];
	}
}
__global__ void assign (int *a, int * b, int * d, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		if ((a[i] & 1) == 1) {
			d[b[i] - 1] = a[i];
		}
	}
}

int reader (int * a, int argc, char ** argv) {
	FILE * fp;
	int N = 0;
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error open file\n");
		exit(1);
	}
	while (fscanf(fp, "%d,", &a[N]) != EOF) {
		++N;
	}
	fclose(fp);
	return N;
}

void parta (int * a, int N) {
	//cudaMalloc:
	int b[N];
	int * d_a;
	int * d_b;
	int * d_tempb;
	int * d_d;
	int size_int = sizeof (int);

	cudaMalloc ((void **) & d_a, N * size_int);
	cudaMalloc ((void **) & d_b, N * size_int);
	cudaMalloc ((void **) & d_tempb, N * size_int);
	cudaMalloc ((void **) & d_d, N * size_int);

	cudaMemcpy (d_a, a, N * size_int, cudaMemcpyHostToDevice);

	//mark b [i] if a[i] is odd
	markOdd<<<(N + BLOCKSIZE - 1) / BLOCKSIZE, BLOCKSIZE>>> (d_a, d_b, N);

	//caculate b[i] : current odd num, inclusive
	for (int k = 1; k < N; k = k*2) {
		cudaMemcpy (d_tempb, d_b, N * size_int, cudaMemcpyDeviceToDevice);
		scan<<<(N + BLOCKSIZE - 1) / BLOCKSIZE, BLOCKSIZE>>> (d_b, d_tempb, k, N);
		cudaDeviceSynchronize();
	}


	cudaMemcpy (b, d_b, N * size_int, cudaMemcpyDeviceToHost);

	//assign d, with d[b[i] - 1] = a[i]
	int dsize = b[N - 1];
	int d[dsize];
	
	assign<<<(N + BLOCKSIZE - 1) / BLOCKSIZE, BLOCKSIZE>>> (d_a, d_b, d_d, N);

	cudaMemcpy (d, d_d, dsize * size_int, cudaMemcpyDeviceToHost);



	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_tempb);
	cudaFree(d_d);
	


	FILE * fp;
	fp = fopen("q3.txt", "w");
    for (int i = 0; i < N; i++) {
		fprintf(fp, "%d, ", b[i]);
    }
    fprintf(fp, "\n");
    for (int i = 0; i < dsize; ++i) {
    	fprintf(fp, "%d,", d[i]);
    }
    fclose(fp);
}


int main (int argc, char ** argv) {

	// printf("Hello World\n");
	//read:a
	int a[MAXSIZE];
	int N = reader(a, argc, argv);
	parta(a, N);
	return 0;
}
