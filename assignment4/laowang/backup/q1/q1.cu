#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 100000
#define BLOCKSIZE 64
#define MAXNUM 2000

__global__ void compare(int * a, bool * b, int N) {
	int i = blockIdx.x / N;
	int j = blockIdx.x % N;
	if ((a[i] < a[j]) || ((a[i] == a[j]) && (i < j)))
		b[j] = true;
}

__global__ void compute(int * a, bool * b, int * min) {
	if (! b[blockIdx.x])
		* min = a[blockIdx.x];
}

__global__ void getLastDigit (int * a, int * b) {
	b[blockIdx.x] = a[blockIdx.x] % 10;
}

int main (int argc, char ** argv) {

	printf("Hello World\n");
	//read:
	int a[MAXSIZE];

	int N = 0;
	FILE * fp;
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error open file\n");
		exit(1);
	}
	while (fscanf(fp, "%d,", &a[N]) != EOF) {
		++N;
	}
	fclose(fp);

	int min;
	int b[N];
	//cudaMalloc:
	int * d_a;
	int * d_bb;
	bool * d_b;
	int * d_min;

	int size_int = sizeof (int);
	int size_bool = sizeof (bool);

	cudaMalloc ((void **) & d_a, N * size_int);
	cudaMalloc ((void **) & d_b, N * size_bool);
	cudaMalloc ((void **) & d_bb, N * size_int);
	cudaMalloc ((void **) & d_min, size_int);

	cudaMemcpy (d_a, &a, N * size_int, cudaMemcpyHostToDevice);
	compare<<<N * N, 1>>> (d_a, d_b, N);
	cudaDeviceSynchronize();
	compute<<<N, 1>>> (d_a, d_b, d_min);
	cudaDeviceSynchronize();
	getLastDigit<<<N, 1>>> (d_a, d_bb);

	cudaMemcpy (&min, d_min, size_int, cudaMemcpyDeviceToHost);
	cudaMemcpy (b, d_bb, N * size_int, cudaMemcpyDeviceToHost);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_bb);
	cudaFree(d_min);

	printf("Minimum value is %d\n", min);

	fp = fopen("last_digit_output.txt", "w");
    for (int i = 0; i < N; i++) {
		fprintf(fp, "%d, ", b[i]);
    }
    fclose(fp);
	return 0;
}