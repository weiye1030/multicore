#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 1000000
#define BLOCKSIZE 1024

__global__ void markOdd (int * a, int * b, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		if ((a[i] & 1) == 1)
			b[i] = 1; 
		else
			b[i] = 0;
	}
}

__global__ void scan (int * b, int * tempb, int d, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if ((i < N) && (i >= d)) {
		b[i] += tempb[i - d];
	}
}
__global__ void assign (int *a, int * b, int * d, int N) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N) {
		if ((a[i] & 1) == 1) {
			d[b[i] - 1] = a[i];
		}
	}
}

int reader (int * a, int argc, char ** argv) {
	FILE * fp;
	int N = 0;
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error open file\n");
		exit(1);
	}
	while (fscanf(fp, "%d,", &a[N]) != EOF) {
		++N;
	}
	fclose(fp);
	return N;
}

void parta (int * a, int N) {
	//cudaMalloc:
	int b[N];
	int dsize = 0;
	for (int i = 0; i < N; ++i) {
		if (a[i] % 2 == 1) {
			b[dsize] = a[i];
			++dsize;
		}
	}


	FILE * fp;
	fp = fopen("q3_test.txt", "w");
    for (int i = 0; i < dsize - 1; ++i) {
    	fprintf(fp, "%d,", b[i]);
    }
    fprintf(fp, "%d\n", b[dsize - 1]);
    fclose(fp);
}


int main (int argc, char ** argv) {

	// printf("Hello World\n");
	//read:a
	int a[MAXSIZE];
	int N = reader(a, argc, argv);
	parta(a, N);
	return 0;
}

