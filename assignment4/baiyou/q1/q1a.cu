/*************************************************************************
	> File Name: q1a.cu
	> Author: 
	> Mail: 
	> Created Time: Wed 26 Oct 2016 02:25:12 PM CDT
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 100000
#define BLOCKSIZE 64
#define MAXNUM 2000

__global__ void minArray(int *g_idata, int *g_odata, int size){
    extern __shared__ int sdata[];

    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i < size){
        sdata[tid] = g_idata[i];
    }
    else{
        sdata[tid] = MAXNUM;
    }
    __syncthreads();

    for(unsigned int s = 1; s < blockDim.x; s *= 2){
        if(tid%(2*s) == 0){
            if(sdata[tid] > sdata[tid+s])
                sdata[tid] = sdata[tid+s];
        }
        __syncthreads();
    }
    if(tid == 0)g_odata[blockIdx.x] = sdata[0];

}

int main(int argc, char** argv){

    int arrayA[MAXSIZE];
    int size = 0;
    FILE *fp;
    fp = fopen(argv[1], "r");
    if(fp == NULL){
        printf("Error open file\n");
        exit(1);
    }    
    while(fscanf(fp, "%d,", &arrayA[size]) != EOF){
        size++;
    }
    fclose(fp);
    printf("Array size is %d\n", size);
    //size = 32;
    int msize = size * sizeof(int);
    int *dm;
    cudaMalloc((void**)&dm, msize);
    cudaMemcpy(dm, arrayA, msize, cudaMemcpyHostToDevice);
    int *drs;
    int curr_size = size;
    int prev_size;

    while(curr_size != 1){
        prev_size = curr_size;
        curr_size = (curr_size+BLOCKSIZE-1)/BLOCKSIZE;
        cudaMalloc((void**)&drs, curr_size*sizeof(int)); 
        minArray<<<curr_size, BLOCKSIZE, BLOCKSIZE>>>(dm, drs, prev_size);
        cudaThreadSynchronize();
        cudaFree(dm);
        dm = drs;
    }
    
    int *hrs;
    hrs = (int *)malloc(sizeof(int));
    cudaMemcpy(hrs, drs, sizeof(int), cudaMemcpyDeviceToHost);

    
    printf("Minimum value is %d\n", *hrs);
    
    cudaFree(drs);
    free(hrs);

}
