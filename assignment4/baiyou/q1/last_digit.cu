#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <string.h>
#include <math.h>
#include <vector>

__global__ void last_digit_kernel(int * d_out, int * d_in, int n) {

    int myId = threadIdx.x + blockIdx.x * blockDim.x;

    if (myId < n) {
	d_out[myId] = d_in[myId] % 10;
    }
}

int main(int argc, char ** argv) {
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    if (deviceCount == 0) {
        fprintf(stderr, "error: no devices supporting CUDA.\n");
        exit(EXIT_FAILURE);
    }
    int dev = 0;
    cudaSetDevice(dev);

    cudaDeviceProp devProps;
    if (cudaGetDeviceProperties(&devProps, dev) == 0)
    {
        printf("Using device %d:\n", dev);
        printf("%s; global mem: %dB; compute v%d.%d; clock: %d kHz\n",
               devProps.name, (int)devProps.totalGlobalMem,
               (int)devProps.major, (int)devProps.minor,
               (int)devProps.clockRate);
    }

    FILE * fp = fopen("inp.txt", "r");
    //FILE * fp = fopen("test.txt", "r");
    char line[1000000];
    char * pch;
    std::vector<int> myArray;
    if (fp != NULL) {
        if (fgets(line, sizeof line, fp) != NULL) {
            pch = strtok(line, ",");
            while (pch != NULL) {
                myArray.push_back(atoi(pch));
                pch = strtok(NULL, ",");
            }
        }
        fclose(fp);
    }
    int ARRAY_SIZE = myArray.size();
    int ARRAY_BYTES = ARRAY_SIZE * sizeof(int);
    int * arr = (int *)malloc(ARRAY_BYTES);
    std::copy(myArray.begin(), myArray.end(), arr);
    printf("number of array elements: %d\n", ARRAY_SIZE);

    int * d_in, * d_out;
    cudaMalloc((void **) &d_in, ARRAY_BYTES);
    cudaMalloc((void **) &d_out, ARRAY_BYTES);

    cudaMemcpy(d_in, arr, ARRAY_BYTES, cudaMemcpyHostToDevice);
    //int threads = sqrt(ARRAY_SIZE);
    int threads = 1;
    int blocks = (ARRAY_SIZE + threads - 1) / threads;
    last_digit_kernel<<<blocks, threads>>>(d_out, d_in, ARRAY_SIZE);

    int * arr_out = (int *)malloc(ARRAY_BYTES);
    cudaMemcpy(arr_out, d_out, ARRAY_BYTES, cudaMemcpyDeviceToHost);

    fp = fopen("output.txt", "w");
    for (int i = 0; i < ARRAY_SIZE; i++) {
    if (i < ARRAY_SIZE - 1)
	fprintf(fp, "%d, ", arr_out[i]);
    else 
	fprintf(fp, "%d", arr_out[i]);
    }
    fclose(fp);

    cudaFree(d_in);
    cudaFree(d_out);

    free(arr);   
    free(arr_out);   

    return 0;
}
