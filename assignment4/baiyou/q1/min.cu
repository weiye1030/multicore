#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <string.h>
#include <math.h>
#include <vector>

__global__ void reduce_kernel(int * d_out, int * d_in, int n) {
    extern __shared__ int sdata[];

    int myId = threadIdx.x + blockIdx.x * blockDim.x;
    int tid = threadIdx.x;

    sdata[tid] = (myId < n) ? d_in[myId] : 1000;
    __syncthreads();

    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
	if (tid < s) {
            if (sdata[tid + s] < sdata[tid])
	        sdata[tid] = sdata[tid + s];
	    }
	__syncthreads();
    }

    if (tid == 0) {
	d_out[blockIdx.x] = sdata[0];
    }
}

int nearestPowerOf2 (int n) {
    if (!n) return n;

    int x = 1;
    while(x < n) x <<= 1;
    return x;
}

void reduce(int * d_out, int * d_intermediate, int * d_in, int size) {
    int threads = nearestPowerOf2(sqrt(size));
    int blocks = (size + threads - 1) / threads;
    printf("num of threads, blocks and array size: %d, %d, %d\n", threads, blocks, size);
    reduce_kernel<<<blocks, threads, threads * sizeof(int)>>>(d_intermediate, d_in, size);

    threads = nearestPowerOf2(blocks);
    printf("num of threads, blocks and array size: %d, 1, %d\n", threads, blocks);
    reduce_kernel<<<1, threads, threads * sizeof(int)>>>(d_out, d_intermediate, blocks);
}

int main(int argc, char ** argv) {
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    if (deviceCount == 0) {
        fprintf(stderr, "error: no devices supporting CUDA.\n");
        exit(EXIT_FAILURE);
    }
    int dev = 0;
    cudaSetDevice(dev);

    cudaDeviceProp devProps;
    if (cudaGetDeviceProperties(&devProps, dev) == 0)
    {
        printf("Using device %d:\n", dev);
        printf("%s; global mem: %dB; compute v%d.%d; clock: %d kHz\n",
               devProps.name, (int)devProps.totalGlobalMem,
               (int)devProps.major, (int)devProps.minor,
               (int)devProps.clockRate);
    }

    FILE * fp = fopen("inp.txt", "r");
    char line[1000000];
    char * pch;
    std::vector<int> myArray;
    if (fp != NULL) {
        if (fgets(line, sizeof line, fp) != NULL) {
            pch = strtok(line, ",");
            while (pch != NULL) {
                myArray.push_back(atoi(pch));
                pch = strtok(NULL, ",");
            }
        }
        fclose(fp);
    }
    int ARRAY_SIZE = myArray.size();
    int ARRAY_BYTES = ARRAY_SIZE * sizeof(int);
    int * arr = (int *)malloc(ARRAY_BYTES);
    std::copy(myArray.begin(), myArray.end(), arr);
    printf("number of array elements: %d\n", ARRAY_SIZE);
    //for (int i = 0; i < ARRAY_SIZE; i++)
    //	    printf("%d\n", arr[i]);

    int * d_in, * d_intermediate, * d_out;
    cudaMalloc((void **) &d_in, ARRAY_BYTES);
    cudaMalloc((void **) &d_intermediate, ARRAY_BYTES);
    cudaMalloc((void **) &d_out, sizeof(int));

    cudaMemcpy(d_in, arr, ARRAY_BYTES, cudaMemcpyHostToDevice);
    reduce(d_out, d_intermediate, d_in, ARRAY_SIZE);

    int minVal;
    cudaMemcpy(&minVal, d_out, sizeof(int), cudaMemcpyDeviceToHost);
    printf("min value returned by device: %d\n", minVal);

#if 0
    int min[4];
    cudaMemcpy(&min, d_intermediate, sizeof(int)*4, cudaMemcpyDeviceToHost);
    for (int i = 0; i < 4; i++)
       printf("%d\n", min[i]);
#endif

    cudaFree(d_in);
    cudaFree(d_intermediate);
    cudaFree(d_out);

    free(arr);   

    return 0;
}
