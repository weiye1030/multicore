package submit1;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Shengwei_Wang on 9/5/16.
 */

interface Lock {
    public void requestCS(int i);
    public void releaseCS(int i);
}

class PetersonTournament implements Lock{
    AtomicInteger[] turn;
    AtomicInteger[] flag;
    volatile int n;
    volatile int p;
    public PetersonTournament (int n) {
        p = 1;
        while((1<<p) < n)
            ++p;
        turn = new AtomicInteger[(1 << p) - 1];
        flag = new AtomicInteger[n];
        for(int i = 0; i < n; ++i){

            flag[i] = new AtomicInteger();
        }
        for(int i = 0; i < (1 << p) - 1; ++i) {
            turn[i] = new AtomicInteger();
        }

        this.n = n;
    }

    public void requestCS(int i){
//        flag[i] = 1;
//        turn[0] = i;
//        for(int j = 0; j < 2; j++)
//            while(i != j && flag[j] >= 1 && turn[0] == i){}
        for(int k = 1; k <= p; ++k){
            flag[i].set(k);
            int l = (1<<(p - k)) - 1 + (i >> k);
            int role = (i >> (k - 1)) & 1;
            turn[l].set(role);
            int start = (i >> k) << k;
            int end = Math.min(start + (1 << k) , n);
//            System.out.println("current i : " + i + "  k: " + k + "  l: "  + l + " start: " + start + " end:" + end + " turn" + turn[l]);
            for(int j = start; j < end; ++j){
//                System.out.println(j + " waiting: id is " + i + "  level is " + k + "  bother id is " + j + " l: " + l + " turn[l]" + turn[l]);
                while(i != j && flag[j].get() >= k && turn[l].get() == role)
                {
//                    System.out.println("waiting: id is " + i + "  level is " + k + "  bother id is " + j + " l: " + l + " turn[l]" + turn[l]);
                }
//                System.out.println("finished waiting " + i);
            }
        }
    }
    public void releaseCS(int i) { flag[i].set(0);}
}

class PIncrement_a extends Thread{
    public static int c = 0;
    public static int parallelIncrement(int c, int numThreads){
        PIncrement_a[] t = new PIncrement_a[numThreads];
        PetersonTournament lock = new PetersonTournament(numThreads);
        PIncrement_a.c = c;
        for(int i = 0; i < numThreads; ++i){
            t[i] = new PIncrement_a(i, 1200000/numThreads, lock); //set thread id = i
            t[i].start();
        }
        for(int i = 0; i < numThreads; ++i){
            try{
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_a.c);

        return PIncrement_a.c;
    }

    int myId;
    int times;
    PetersonTournament lock;
    public PIncrement_a(int id, int times, PetersonTournament lock){
        myId = id;
        this.times = times;
        this.lock = lock;
    }
    public void run() {
        for(int i = 0; i < times; ++i){
            lock.requestCS(myId);
            ++c;
            lock.releaseCS(myId);
        }
    }
}

class PIncrement_b extends Thread {
    public static AtomicInteger c;

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_b.c = new AtomicInteger(c);
        PIncrement_b[] t = new PIncrement_b[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_b(i, 1200000 / numThreads); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_b.c.get());
        return PIncrement_b.c.get();
    }

    int myId;
    int times;

    public PIncrement_b(int id, int times) {
        myId = id;
        this.times = times;
    }

    public void run() {
        for (int i = 0; i < times; ++i) {
            while(true){
                int m = c.get();
                if(c.compareAndSet(m, m + 1))
                    break;
            }

        }
    }
}
class PIncrement_c extends Thread {
    static int c;
    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_c.c = c;
        PIncrement_c[] t = new PIncrement_c[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_c(i, 1200000 / numThreads); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_c.c);
        return PIncrement_c.c;
    }

    int myId;
    int times;

    public PIncrement_c(int id, int times) {
        myId = id;
        this.times = times;
    }
    public synchronized static void incrementC(){
        ++c;
    }
    public void run() {
        for (int i = 0; i < times; ++i) {
            incrementC();
        }
    }
}
class PIncrement_d extends Thread {
    static int c;
    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_d.c = c;
        PIncrement_d[] t = new PIncrement_d[numThreads];
        ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_d(i, 1200000 / numThreads, lock); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_d.c);
        return PIncrement_d.c;
    }

    int myId;
    int times;
    ReentrantLock lock;
    public PIncrement_d(int id, int times, ReentrantLock lock) {
        myId = id;
        this.lock = lock;
        this.times = times;
    }
    public static void incrementC(){
        ++c;
    }
    public void run() {
        for (int i = 0; i < times; ++i) {
            lock.lock();
            incrementC();
            lock.unlock();
        }
    }
}


public class TestPIncrement {
    public static void main(String[] args) throws Exception {
        long start;
        long end;
        int n = 1;
        int c = 0;
        while(n <= 8){
            System.out.println(n + " Threads: ");
            System.out.println("Test for part a: ");
            start = System.currentTimeMillis();
            PIncrement_a.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part a finished Time: " + (end - start));

            System.out.println("Test for part b: ");
            start = System.currentTimeMillis();
            PIncrement_b.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part b finished Time: " + (end - start));


            System.out.println("Test for part c: ");
            start = System.currentTimeMillis();
            PIncrement_c.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part c finished Time: " + (end - start));

            System.out.println("Test for part d: ");
            start = System.currentTimeMillis();
            PIncrement_d.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part d finished Time: " + (end - start));
            n *=2;
        }
    }
}
