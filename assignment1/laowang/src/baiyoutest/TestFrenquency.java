/*************************************************************************
    > File Name: testFrenquency.java
    > Author: Wei Ye
    > Mail: yeweizju@gmail.com
    > Created Time: Sun Sep  4 00:00:52 2016
 ************************************************************************/
public class TestFrenquency {
    public static void main(String[] args) {
        int[] A = new int[]{0, 1, 0, 0, 1,2,3,4,5,6,8,0,0,8,7,0,0,7,7,0,0,7,7,5,4,0};
        int ans = Frenquency.parallelFreq(7, A, 3);
        System.out.println("Answer is " + ans);
    }
}
