package assignment1;
import java.util.concurrent.*;
/**
 * Created by Shengwei_Wang on 9/4/16.
 */
public class Fibonacci2 implements Callable<Integer> {
    public static ExecutorService threadPool = Executors.newCachedThreadPool();
    int n;
    public Fibonacci2(int n) {
        this.n = n;
    }
    public Integer call() {
        try {
            if ((n == 0) || (n == 1)) return 1;
            else {
                Future<Integer> f1 = threadPool.submit(new Fibonacci2(n - 1));
                Future<Integer> f2 = threadPool.submit(new Fibonacci2(n - 2));
                return f1.get() + f2.get();
            }
        } catch (Exception e) {System.err.println(e); return 1;}
    }
}
