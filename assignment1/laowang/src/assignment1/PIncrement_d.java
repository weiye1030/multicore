package assignment1;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Shengwei_Wang on 9/6/16.
 */
public class PIncrement_d extends Thread {
    static int c;
    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_d.c = c;
        PIncrement_d[] t = new PIncrement_d[numThreads];
        ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_d(i, 1200000 / numThreads, lock); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_d.c);
        return PIncrement_d.c;
    }

    int myId;
    int times;
    ReentrantLock lock;
    public PIncrement_d(int id, int times, ReentrantLock lock) {
        myId = id;
        this.lock = lock;
        this.times = times;
    }
    public static void incrementC(){
        ++c;
    }
    public void run() {
        for (int i = 0; i < times; ++i) {
            lock.lock();
            incrementC();
            lock.unlock();
        }
    }
}
