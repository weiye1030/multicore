package assignment1;

/**
 * Created by Shengwei_Wang on 9/6/16.
 */
public class PetersonAlgorithm implements Lock {
    volatile boolean wantCS[] = {false, false};
    volatile int turn = 1;
    public void requestCS(int i){
        int j = 1 - i;
        wantCS[i] = true;
        turn = j;
        while(wantCS[j] && (turn == j)) ;
    }

    public void releaseCS(int i){
        wantCS[i] = false;
    }
}
