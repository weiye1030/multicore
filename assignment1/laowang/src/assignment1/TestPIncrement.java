package assignment1;

/**
 * Created by Shengwei_Wang on 9/8/16.
 */
public class TestPIncrement {
    public static void main(String[] args) throws Exception {
        long start;
        long end;
        int n = 1;
        int c = 0;
        while(n <= 8){
            System.out.println(n + " Threads: ");
            System.out.println("Test for part a: ");
            start = System.currentTimeMillis();
            PIncrement_a.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part a finished Time: " + (end - start));

            System.out.println("Test for part b: ");
            start = System.currentTimeMillis();
            PIncrement_b.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part b finished Time: " + (end - start));


            System.out.println("Test for part c: ");
            start = System.currentTimeMillis();
            PIncrement_c.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part c finished Time: " + (end - start));

            System.out.println("Test for part d: ");
            start = System.currentTimeMillis();
            PIncrement_d.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part d finished Time: " + (end - start));
            n *=2;
        }
    }
}
