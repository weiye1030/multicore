package assignment1;



import java.util.Random;

public class MyThread extends Thread {
    int myId;
    Lock lock;
    Random r = new Random();

    public MyThread(int id, Lock lock) {
        myId = id;
        this.lock = lock;
    }

    void nonCriticalSection() {
        System.out.println(myId + " is not in CS");
        try {
            Thread.sleep(r.nextInt(10));
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    void CriticalSection() {
        System.out.println(myId + " is in CS *****");
        try {
            Thread.sleep(r.nextInt(10));
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void run() {
        while (true) {
            lock.requestCS(myId);
            CriticalSection();
            lock.releaseCS(myId);
            nonCriticalSection();
        }
    }

    public static void main(String[] args) throws Exception {
//        MyThread[] t;
//        int N = 2;
//        t = new MyThread[N];
//        Lock lock = new PetersonAlgorithm();
////        Lock lock = new PetersonAlgorithmLock(N); //lock type
//        for (int i = 0; i < N; ++i) {
//            t[i] = new MyThread(i, lock);
//            t[i].start();
//        }
//        PIncrement_b.parallelIncrement(5, 3);
//        PIncrement_c.parallelIncrement(4, 5);
        long start;
        long end;
        int c = 20;
        int n = 5;
        for(n = 1; n <= 8; ++n) {
            System.out.println(n + " Threads: ");
            System.out.println("Test for part a: ");
            start = System.currentTimeMillis();
            PIncrement_a.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part a finished Time: " + (end - start));

            System.out.println("Test for part b: ");
            start = System.currentTimeMillis();
            PIncrement_b.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part b finished Time: " + (end - start));


            System.out.println("Test for part c: ");
            start = System.currentTimeMillis();
            PIncrement_c.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part c finished Time: " + (end - start));

            System.out.println("Test for part d: ");
            start = System.currentTimeMillis();
            PIncrement_d.parallelIncrement(c, n);
            end = System.currentTimeMillis();
            System.out.println("part d finished Time: " + (end - start));
        }
    }
}
	// write your code here
//        System.out.println("122");
//        System.out.println(PIncrement_a.parallelIncrement(5, 2));
//        try{
//            ExecutorService es = Executors.newSingleThreadExecutor();
//            int[] a = {0,1,2,3,4,1,2,3,4,2,3,4};
//
//            System.out.println("Answer is " + Frequency.prallelFreq(1, a, 3));
//            es.shutdown();
//        } catch (Exception e) {System.err.println(e);}
//        try{
//            ExecutorService es = Executors.newSingleThreadExecutor();
//            Fibonacci2 f = new Fibonacci2(6);
//            Future<Integer> f1 = es.submit(f);
//            System.out.println("Answer is " + f1.get());
//            es.shutdown();
//            f.threadPool.shutdown();
//        } catch (Exception e) {System.err.println(e);}
