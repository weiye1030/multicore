package assignment1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Shengwei_Wang on 9/4/16.
 */
class PetersonAlgorithmLock implements Lock{
    AtomicInteger[] last;
    AtomicInteger[] gate;
    int n;
    public PetersonAlgorithmLock (int n) {
        last = new AtomicInteger[n];
        gate = new AtomicInteger[n];
        for(int i = 0; i < n; ++i){
            last[i] = new AtomicInteger();
            gate[i] = new AtomicInteger();
        }
        this.n = n;
    }

    public void requestCS(int i){
        for(int k = 1; k < n; ++k){
//            System.out.println("pid:" + i + " gate:" + k);
            gate[i].set(k);
            last[k].set(i);
            for(int j = 0; j < n; ++j){
                while(i != j && gate[j].get() >= k && last[k].get() == i)
                {
//                    System.out.println("waiting: id is " + i + "  gate is " + k + "  bother id is " + j + "  last[k] is " + last[k] + "  This pointer" + this);
                }
            }
        }
    }
    public void releaseCS(int i) {
        gate[i].set(0);
    }
}
