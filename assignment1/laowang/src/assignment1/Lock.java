package assignment1;

/**
 * Created by Shengwei_Wang on 9/12/16.
 */
public interface Lock {
    public void requestCS(int i);
    public void releaseCS(int i);
}
