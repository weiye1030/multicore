package assignment1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Shengwei_Wang on 9/4/16.
 */
public class Frequency implements Callable<Integer> {
    public static ExecutorService threadPool = Executors.newCachedThreadPool();
    int l, r;
    int x;
    int[] A;
    public static int prallelFreq(int x, int[] A, int numTreads) {
        int count = 0;
        int interval;
        if(A.length < numTreads){
            interval = 1;
            numTreads = A.length;
        } else {
            interval = Math.max(A.length / numTreads, 1);
        }
        try {


            Future<Integer>[] f = new Future[numTreads];
            int i;
            for (i = 0; i < numTreads - 1; ++i) {
                f[i] = threadPool.submit(new Frequency(i * interval, (i + 1) * interval, x, A));
            }
            f[i] = threadPool.submit(new Frequency(i * interval, A.length, x, A));
            for (i = 0; i < numTreads; ++i) {
                count += f[i].get();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return count;
    }

    public Frequency(int l, int r, int x, int[] A){
        this.l = l;
        this.r = r;
        this.x = x;
        this.A = A;
    }
    public Integer call(){
        int count = 0;
        for(int i = l; i < r; ++i){
            if(A[i] == x)
                ++count;
        }
        return count;
    }
}
