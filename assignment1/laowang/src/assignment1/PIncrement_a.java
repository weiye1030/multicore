package assignment1;

/**
 * Created by Shengwei_Wang on 9/4/16.
 */

public class PIncrement_a extends Thread{
    public static int c = 0;
    public static int parallelIncrement(int c, int numThreads){
        PIncrement_a[] t = new PIncrement_a[numThreads];
        PetersonTournament lock = new PetersonTournament(numThreads);
        PIncrement_a.c = c;
        for(int i = 0; i < numThreads; ++i){
            t[i] = new PIncrement_a(i, 1200000/numThreads, lock); //set thread id = i
            t[i].start();
        }
        for(int i = 0; i < numThreads; ++i){
            try{
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_a.c);

        return PIncrement_a.c;
    }

    int myId;
    int times;
    PetersonTournament lock;
    public PIncrement_a(int id, int times, PetersonTournament lock){
        myId = id;
        this.times = times;
        this.lock = lock;
    }
    public void run() {
        for(int i = 0; i < times; ++i){
            lock.requestCS(myId);
            ++c;
            lock.releaseCS(myId);
        }
    }
}



