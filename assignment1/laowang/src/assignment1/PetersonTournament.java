package assignment1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Shengwei_Wang on 9/5/16.
 */
class PetersonTournament implements Lock{
    AtomicInteger[] turn;
    AtomicInteger[] flag;
    volatile int n;
    volatile int p;
    public PetersonTournament (int n) {
        p = 1;
        while((1<<p) < n)
            ++p;
        turn = new AtomicInteger[(1 << p) - 1];
        flag = new AtomicInteger[n];
        for(int i = 0; i < n; ++i){

            flag[i] = new AtomicInteger();
        }
        for(int i = 0; i < (1 << p) - 1; ++i) {
            turn[i] = new AtomicInteger();
        }

        this.n = n;
    }

    public void requestCS(int i){
//        flag[i] = 1;
//        turn[0] = i;
//        for(int j = 0; j < 2; j++)
//            while(i != j && flag[j] >= 1 && turn[0] == i){}
        for(int k = 1; k <= p; ++k){
            flag[i].set(k);
            int l = (1<<(p - k)) - 1 + (i >> k);
            int role = (i >> (k - 1)) & 1;
            turn[l].set(role);
            int start = (i >> k) << k;
            int end = Math.min(start + (1 << k) , n);
//            System.out.println("current i : " + i + "  k: " + k + "  l: "  + l + " start: " + start + " end:" + end + " turn" + turn[l]);
            for(int j = start; j < end; ++j){
//                System.out.println(j + " waiting: id is " + i + "  level is " + k + "  bother id is " + j + " l: " + l + " turn[l]" + turn[l]);
                while(i != j && flag[j].get() >= k && turn[l].get() == role)
                {
//                    System.out.println("waiting: id is " + i + "  level is " + k + "  bother id is " + j + " l: " + l + " turn[l]" + turn[l]);
                }
//                System.out.println("finished waiting " + i);
            }
        }
    }
    public void releaseCS(int i) { flag[i].set(0);}
}
