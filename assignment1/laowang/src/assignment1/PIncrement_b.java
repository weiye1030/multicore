package assignment1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Shengwei_Wang on 9/6/16.
 */
public class PIncrement_b extends Thread {
    public static AtomicInteger c;

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_b.c = new AtomicInteger(c);
        PIncrement_b[] t = new PIncrement_b[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_b(i, 1200000 / numThreads); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_b.c.get());
        return PIncrement_b.c.get();
    }

    int myId;
    int times;

    public PIncrement_b(int id, int times) {
        myId = id;
        this.times = times;
    }

    public void run() {
        for (int i = 0; i < times; ++i) {
            while(true){
                int m = c.get();
                if(c.compareAndSet(m, m + 1))
                    break;
            }

        }
    }
}
