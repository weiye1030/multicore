package assignment1;

/**
 * Created by Shengwei_Wang on 9/6/16.
 */
public class PIncrement_c extends Thread {
    static int c;
    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_c.c = c;
        PIncrement_c[] t = new PIncrement_c[numThreads];
        for (int i = 0; i < numThreads; ++i) {
            t[i] = new PIncrement_c(i, 1200000 / numThreads); //set thread id = i
            t[i].start();
        }
        for (int i = 0; i < numThreads; ++i) {
            try {
                t[i].join();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        System.out.println(PIncrement_c.c);
        return PIncrement_c.c;
    }

    int myId;
    int times;

    public PIncrement_c(int id, int times) {
        myId = id;
        this.times = times;
    }
    public synchronized static void incrementC(){
        ++c;
    }
    public void run() {
        for (int i = 0; i < times; ++i) {
            incrementC();
        }
    }
}

