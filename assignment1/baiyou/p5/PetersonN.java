import java.util.concurrent.atomic.AtomicInteger;

class PetersonN implements Lock {
    int N;
    AtomicInteger[] gate; 
    AtomicInteger[] last; 
    public PetersonN(int numProc) {
        N = numProc;
        gate = new AtomicInteger[N];//We only use gate[1]->gate[N-1]; gate[0] is unused
        last = new AtomicInteger[N];
        for (int i = 0; i < N; i++) {
            gate[i] = new AtomicInteger();
            last[i] = new AtomicInteger();
        }
    }
    
    public void requestCS(int i) {
        for (int k = 1; k < N; k++) { 
            gate[i].set(k); 
            last[k].set(i);
            for (int j = 0; j < N; j++) {
                while ((j != i) &&  // there is some other process
                        (gate[j].get() >= k) &&  // that is ahead or at the same level
                        (last[k].get() == i)) // and I am the last to update last[k]
                {};// busy wait
            }
        }
    }
    public void releaseCS(int i) {
        gate[i].set(0);
    }
}
