import java.util.concurrent.atomic.AtomicInteger;

public class PIncrement_b extends Thread {
    static AtomicInteger c = new AtomicInteger();
    int increment;
    public PIncrement_b(int k) {
        increment = k;
    }
    public void run() {
        for (int k = 0; k < increment; k++) {
            while(true) {
                int tmp = c.get();
                if(c.compareAndSet(tmp, tmp + 1)) break;
            }
        }
    }

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_b.c.set(c);
        PIncrement_b t[] = new PIncrement_b[numThreads];
        for (int i = 0; i < numThreads; i++) {
            int increment = 1200000/numThreads;
            if (i == numThreads - 1)
                increment = 1200000 -  1200000 / numThreads * (numThreads - 1);
            t[i] = new PIncrement_b(increment);
            t[i].start();
        }
        try {
            for (int i = 0; i < numThreads; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {};
        
        return PIncrement_b.c.get();
    }
}
