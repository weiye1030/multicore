import java.util.concurrent.atomic.AtomicInteger;

public class PetersonTournamentLock implements Lock {
	int N;
	int Level;
	int NumNode;
	AtomicInteger[] gate;
	AtomicInteger[] turn;
	public PetersonTournamentLock(int numProc) {
		N = numProc;
		Level = (int) Math.ceil(Math.log(N)/Math.log(2));
        NumNode = (int) Math.pow(2, Level);

        //System.out.println("hehe" + N + " " + NumNode);
		gate = new AtomicInteger[N];
		for (int j = 0; j < N; j++) {
			gate[j] = new AtomicInteger();
        }
		turn = new AtomicInteger[NumNode];
		for (int j = 0; j < NumNode; j++) {
			turn[j] = new AtomicInteger();
		}
	}

	public void requestCS(int i) {
		int nodeId = i + NumNode;
		for (int k = 1; k <= Level; k++) {
			gate[i].set(k);
            int role = nodeId % 2;
            int gateId = nodeId / 2;
			turn[gateId].set(role);
            int left = gateId * (int) Math.pow(2, k) - NumNode;
            int right = Math.min(left + (int) Math.pow(2, k), N);
            //System.out.println(i + " " + k + " " + left + " " + right + " " + gateId);
            for (int j = left; j < right; j++) {
                while(i != j && 
                        turn[gateId].get() == role  && 
                        gate[j].get() >= k) ;
            }
            nodeId = gateId;
        }
    }

    public void releaseCS(int i) {
        gate[i].set(0);
    }
}
