public class PIncrement_a extends Thread {
    static int c;
    int increment;
    int myId;
    Lock lock;
    public PIncrement_a(int i, int k, Lock lock) {
        myId = i;
        increment = k;
        this.lock = lock;
    }
    public void run() {
        for (int k = 0; k < increment; k++) {
            lock.requestCS(myId);
            ++c;
            lock.releaseCS(myId);
        }
    }

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_a.c = c;
        PIncrement_a t[] = new PIncrement_a[numThreads];
        //Lock lock = new PetersonN(numThreads);
        Lock lock = new PetersonTournamentLock(numThreads);
        for (int i = 0; i < numThreads; i++) {
            int increment = 1200000/numThreads;
            if (i == numThreads - 1)
                increment = 1200000 -  1200000 / numThreads * (numThreads - 1);
            t[i] = new PIncrement_a(i, increment, lock);
            t[i].start();
        }
        try {
            for (int i = 0; i < numThreads; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {};
        
        return PIncrement_a.c;
    }
}
