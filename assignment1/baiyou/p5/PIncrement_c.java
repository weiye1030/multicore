public class PIncrement_c extends Thread {
    static int c;
    int increment;
    public PIncrement_c(int k) {
        increment = k;
    }
    public void run() {
        for (int k = 0; k < increment; k++) {
            incre();
        }
    }
    public synchronized static void incre() {
        ++c;
    }

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_c.c = c;
        PIncrement_c t[] = new PIncrement_c[numThreads];
        for (int i = 0; i < numThreads; i++) {
            int increment = 1200000/numThreads;
            if (i == numThreads - 1)
                increment = 1200000 -  1200000 / numThreads * (numThreads - 1);
            t[i] = new PIncrement_c(increment);
            t[i].start();
        }
        try {
            for (int i = 0; i < numThreads; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {};
        
        return PIncrement_c.c;
    }
}
