import java.util.concurrent.locks.ReentrantLock;

public class PIncrement_d extends Thread {
    static int c;
    int increment;
    ReentrantLock lock;
    public PIncrement_d(int k, ReentrantLock lock) {
        increment = k;
        this.lock = lock;
    }
    public void run() {
        for (int k = 0; k < increment; k++) {
            lock.lock();
            try{
                ++c;
            } finally {
                lock.unlock();
            }
        }
    }

    public static int parallelIncrement(int c, int numThreads) {
        PIncrement_d.c = c;
        PIncrement_d t[] = new PIncrement_d[numThreads];
        ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < numThreads; i++) {
            int increment = 1200000/numThreads;
            if (i == numThreads - 1)
                increment = 1200000 -  1200000 / numThreads * (numThreads - 1);
            t[i] = new PIncrement_d(increment, lock);
            t[i].start();
        }
        try {
            for (int i = 0; i < numThreads; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {};
        
        return PIncrement_d.c;
    }
}
