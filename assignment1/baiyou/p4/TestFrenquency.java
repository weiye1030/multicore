/*************************************************************************
    > File Name: testFrenquency.java
    > Author: Wei Ye
    > Mail: yeweizju@gmail.com
    > Created Time: Sun Sep  4 00:00:52 2016
 ************************************************************************/
public class TestFrenquency {
    public static void main(String[] args) {
        int[] A = new int[]{0, 1, 0, 0, 1};
        int ans = Frenquency.parallelFreq(0, A, 3);
        System.out.println("Answer is " + ans);
    }
}
