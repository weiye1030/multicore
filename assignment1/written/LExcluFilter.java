import java.util.Arrays;

class LExcluFilter implements Lock {
    int N;
    int l;
    volatile int[] gate; 
    volatile int[] last; 
    public LExcluFilter(int l, int numProc) {
        N = numProc;
        this.l = l;
        gate = new int[N];//We only use gate[1]->gate[N-l]
        Arrays.fill(gate, 0);
        last = new int[N];
        Arrays.fill(last, 0);
    }
    public void requestCS(int i) {
        for (int k = 1; k <= N - l; k++) { 
            gate[i] = k; 
            last[k] = i;
            int cnt = 0;
            for (int j = 0; j < N; j++) {
                if (j != i && gate[j] >= k)
                    cnt++;
                while ((cnt >= l) &&  // there are l processes
                        (last[k] == i)) // and I am the last to update last[k]
                {};// busy wait
            }
        }
    }
    public void releaseCS(int i) {
        gate[i] = 0;
    }
}
