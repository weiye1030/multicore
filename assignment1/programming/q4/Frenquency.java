/*************************************************************************
    > File Name: Frenquency.java
    > Author: Wei Ye
    > Mail: yeweizju@gmail.com
    > Created Time: Sun 04 Sep 2016 11:27:13 AM CDT
 ************************************************************************/
import java.util.concurrent.*;
import java.util.Arrays;
import java.util.ArrayList;

public class Frenquency implements Callable<Integer> {
    int[] array;
    int x;
    public Frenquency(int x, int[] A) {
        this.x = x;
        this.array = A;
    }

    public Integer call() {
        try {
            int cnt = 0;
            for (int i : array) {
                if (i == x) cnt++;
            }
            return cnt;
        } catch (Exception e) {
            System.err.println(e);
            return 1;
        }
    }

    public static int parallelFreq(int x, int[] A, int numThreads) {
        try {
            ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
            ArrayList<Future<Integer>> futures = new ArrayList<Future<Integer>>(numThreads);

            int size = (int) Math.ceil((double) A.length / numThreads);
            for (int k = 0; k < numThreads; k++) {
                Frenquency f = new Frenquency(x, Arrays.copyOfRange(A, k*size, Math.min((k + 1)*size, A.length)));
                futures.add(threadPool.submit(f));
            }

            int result = 0;
            for (int k = 0; k < numThreads; k++) {
                result += futures.get(k).get();
            }
            threadPool.shutdown();
            return result;
        } catch (Exception e) { System.err.println(e); return 1; }
    }
}
